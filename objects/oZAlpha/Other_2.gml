/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0BBDBC81
/// @DnDArgument : "code" "/// @desc full-screen toggle$(13_10)///set full-screen$(13_10)if keyboard_check(vk_f5)$(13_10)   {$(13_10)   if window_get_fullscreen()$(13_10)      {$(13_10)      window_set_fullscreen(false);$(13_10)      }$(13_10)   else$(13_10)      {$(13_10)      window_set_fullscreen(true);$(13_10)      }$(13_10)   }"
/// @desc full-screen toggle
///set full-screen
if keyboard_check(vk_f5)
   {
   if window_get_fullscreen()
      {
      window_set_fullscreen(false);
      }
   else
      {
      window_set_fullscreen(true);
      }
   }