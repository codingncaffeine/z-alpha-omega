{
    "id": "985946fd-046a-4475-a871-1a7e0d028992",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLaser1",
    "eventList": [
        {
            "id": "c68703eb-a241-4e5b-835a-62314c46a324",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "985946fd-046a-4475-a871-1a7e0d028992"
        },
        {
            "id": "8a248b7a-2340-4d7a-be62-f0baf6f39966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "985946fd-046a-4475-a871-1a7e0d028992"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "cdc3ff15-7f1f-4f9f-801a-cc1a08674f42",
    "visible": true
}