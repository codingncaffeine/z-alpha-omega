{
    "id": "c8891d91-afb3-43bd-8652-3e90ff7bae94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemylaser",
    "eventList": [
        {
            "id": "e02b85c2-67f4-4e0b-b4e6-fa90657d21f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8891d91-afb3-43bd-8652-3e90ff7bae94"
        },
        {
            "id": "e02af3c1-4243-4dad-b2ee-edb798e82b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c8891d91-afb3-43bd-8652-3e90ff7bae94"
        },
        {
            "id": "f3da0627-9da0-464e-99fc-5fa9223f2645",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8891d91-afb3-43bd-8652-3e90ff7bae94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
    "visible": true
}