{
    "id": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyfighter1",
    "eventList": [
        {
            "id": "f53a9211-819f-411c-b378-d2413e3d51fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "985946fd-046a-4475-a871-1a7e0d028992",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704"
        },
        {
            "id": "5567e8b6-10e2-4bf2-981c-f493b0ef6f81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704"
        },
        {
            "id": "a063f27e-2cd6-4168-b484-ff6e835575a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704"
        },
        {
            "id": "32447c87-49cb-4625-b203-ccc8fbc1575d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704"
        },
        {
            "id": "c2b0646c-b1f4-442b-ab84-c3528e36992d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cc2ea8b0-c5fb-4b1d-8aa4-9a453cd57704"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3cf569ba-d592-4782-a29c-b908f702a823",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "5",
            "varName": "hp",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "48ee59ee-3b72-4d06-a3ee-6d75f26862c1",
    "visible": true
}