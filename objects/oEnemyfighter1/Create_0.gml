/// @DnDAction : YoYo Games.Instance Variables.Set_Health
/// @DnDVersion : 1
/// @DnDHash : 26765C48
/// @DnDArgument : "health" "2"

__dnd_health = real(2);

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 718A70A0
/// @DnDArgument : "speed" "-5"
/// @DnDArgument : "type" "1"
hspeed = -5;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 50B4E09A
/// @DnDArgument : "code" "/// @desc bullet speed and follow path$(13_10)$(13_10)path_start(path1, 4, path_action_reverse, 0);$(13_10)$(13_10)/// higher the number longer the delay$(13_10)alarm[1] = (room_speed * 1);"
/// @desc bullet speed and follow path

path_start(path1, 4, path_action_reverse, 0);

/// higher the number longer the delay
alarm[1] = (room_speed * 1);