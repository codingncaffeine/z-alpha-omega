/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5C907FB2
/// @DnDArgument : "code" "/// @desc$(13_10)$(13_10)///change direction of ship when following path$(13_10)if( abs(angle_difference(180, direction)) < 90){$(13_10)image_xscale = -1;$(13_10)}else{$(13_10)image_xscale = 1;$(13_10)}$(13_10)$(13_10)$(13_10)"
/// @desc

///change direction of ship when following path
if( abs(angle_difference(180, direction)) < 90){
image_xscale = -1;
}else{
image_xscale = 1;
}