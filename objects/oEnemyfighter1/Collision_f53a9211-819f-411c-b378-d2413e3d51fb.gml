/// @DnDAction : YoYo Games.Instance Variables.Set_Health
/// @DnDVersion : 1
/// @DnDHash : 21DCB99E
/// @DnDArgument : "health" "-2"

__dnd_health = real(-2);

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 28CC640E
instance_destroy();

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 65EC0A37
/// @DnDArgument : "soundid" "snExplosion1"
/// @DnDSaveInfo : "soundid" "f5a56633-016a-41f8-8f4d-02603ee3082b"
audio_play_sound(snExplosion1, 0, 0);

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 08AA4D63
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "oEnemydead"
/// @DnDSaveInfo : "objectid" "fbb88a76-6e7a-49bf-85f8-949bd03c4902"
instance_create_layer(x + 0, y + 0, "Instances", oEnemydead);

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 26C3657C
/// @DnDApplyTo : c8891d91-afb3-43bd-8652-3e90ff7bae94
with(oEnemylaser) instance_destroy();