{
    "id": "356ddd85-af93-475d-a3b2-ddb41ece75dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTransition",
    "eventList": [
        {
            "id": "06d97dbb-c1da-496f-a3ed-47b8c1c64fe2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "356ddd85-af93-475d-a3b2-ddb41ece75dc"
        },
        {
            "id": "55e739e8-225c-4afe-96be-2f72b59da0ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "356ddd85-af93-475d-a3b2-ddb41ece75dc"
        },
        {
            "id": "ccf05a1f-6b4e-4f48-a3b5-8d6c0f7f21ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "356ddd85-af93-475d-a3b2-ddb41ece75dc"
        },
        {
            "id": "713aa201-3787-4b7d-9ef8-ef099df9d398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "356ddd85-af93-475d-a3b2-ddb41ece75dc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}