/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 0D43AD5B
/// @DnDArgument : "code" "/// @description deadzone, score$(13_10)$(13_10)$(13_10)gamepad_set_axis_deadzone(0, 0.2);$(13_10)$(13_10)score = 0;"
/// @description deadzone, score


gamepad_set_axis_deadzone(0, 0.2);

score = 0;

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 242A0514
/// @DnDArgument : "lives" "3"

__dnd_lives = real(3);

/// @DnDAction : YoYo Games.Instance Variables.Set_Health
/// @DnDVersion : 1
/// @DnDHash : 217DF1D3
/// @DnDArgument : "health" "10"

__dnd_health = real(10);