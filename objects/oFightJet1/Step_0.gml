/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 1C53BB59
/// @DnDArgument : "code" "//keyboard gamepad movement$(13_10)$(13_10)vspeed = 0;$(13_10)hspeed = 0;$(13_10)$(13_10)if keyboard_check(vk_down) || gamepad_axis_value (0, gp_axislv) > 0$(13_10)	{$(13_10)		vspeed = 5;$(13_10)	}$(13_10)$(13_10)if keyboard_check(vk_up) || gamepad_axis_value (0, gp_axislv) < 0$(13_10)	{$(13_10)		vspeed = -5;$(13_10)	}$(13_10)$(13_10)if keyboard_check(vk_left) || gamepad_axis_value (0, gp_axislh) < 0$(13_10)	{$(13_10)		hspeed = -5;$(13_10)	}$(13_10)$(13_10)if keyboard_check(vk_right) || gamepad_axis_value (0, gp_axislh) > 0$(13_10)	{$(13_10)		hspeed = 5;$(13_10)	}$(13_10)$(13_10)$(13_10)$(13_10)///animation $(13_10)if (vspeed > 0)$(13_10)	{$(13_10)		sprite_index = sPlayerdown;$(13_10)		image_speed = 1$(13_10)	}$(13_10)$(13_10)$(13_10)if (vspeed < 0)$(13_10)	{$(13_10)		sprite_index = sPlayerup;$(13_10)		image_speed = 1$(13_10)	}$(13_10)	$(13_10)if (vspeed = 0)$(13_10)	{$(13_10)		sprite_index = sFighter1;$(13_10)	}$(13_10)$(13_10)$(13_10)if ((sprite_index == sPlayerup || sprite_index == sPlayerdown) && scAnimationEnd())$(13_10){$(13_10)    image_speed = 0;$(13_10)}$(13_10)$(13_10)$(13_10)// gamepad firing$(13_10)$(13_10)if gamepad_button_check_pressed(0, gp_face1)$(13_10)	{$(13_10)		instance_create_layer(x,y,"Instances",oLaser1)$(13_10)		audio_play_sound(snLasershot,4,false)$(13_10)		alarm_get(0)$(13_10)	}$(13_10)"
//keyboard gamepad movement

vspeed = 0;
hspeed = 0;

if keyboard_check(vk_down) || gamepad_axis_value (0, gp_axislv) > 0
	{
		vspeed = 5;
	}

if keyboard_check(vk_up) || gamepad_axis_value (0, gp_axislv) < 0
	{
		vspeed = -5;
	}

if keyboard_check(vk_left) || gamepad_axis_value (0, gp_axislh) < 0
	{
		hspeed = -5;
	}

if keyboard_check(vk_right) || gamepad_axis_value (0, gp_axislh) > 0
	{
		hspeed = 5;
	}



///animation 
if (vspeed > 0)
	{
		sprite_index = sPlayerdown;
		image_speed = 1
	}


if (vspeed < 0)
	{
		sprite_index = sPlayerup;
		image_speed = 1
	}
	
if (vspeed = 0)
	{
		sprite_index = sFighter1;
	}


if ((sprite_index == sPlayerup || sprite_index == sPlayerdown) && scAnimationEnd())
{
    image_speed = 0;
}


// gamepad firing

if gamepad_button_check_pressed(0, gp_face1)
	{
		instance_create_layer(x,y,"Instances",oLaser1)
		audio_play_sound(snLasershot,4,false)
		alarm_get(0)
	}