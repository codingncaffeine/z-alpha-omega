/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 6C17C1B7
/// @DnDArgument : "code" "/// @description Collision with Asteroid$(13_10)$(13_10)with (other) $(13_10){$(13_10)	hp = hp -4;$(13_10)	flash = 3;$(13_10)}$(13_10)$(13_10)with (self)$(13_10){$(13_10)	hp = hp -2;$(13_10)	flash = 3;$(13_10)}"
/// @description Collision with Asteroid

with (other) 
{
	hp = hp -4;
	flash = 3;
}

with (self)
{
	hp = hp -2;
	flash = 3;
}