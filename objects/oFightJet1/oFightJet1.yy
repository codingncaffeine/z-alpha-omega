{
    "id": "3bd07771-27dc-47fb-9a42-c3374930626b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oFightJet1",
    "eventList": [
        {
            "id": "1e051c0d-395c-40d5-9c0d-81a610afbde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        },
        {
            "id": "cb62e171-473f-423d-a215-180bfbafc42e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        },
        {
            "id": "922559cb-3ff2-4427-929b-d9eab5a132e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        },
        {
            "id": "73fef1b3-e4f0-4e22-b8c4-cf3c5745ffd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        },
        {
            "id": "6bc5231a-9d9f-46b0-811d-68b1539f5a02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        },
        {
            "id": "8a1e86cd-0e29-4908-818b-df3de8e9d30f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3bd07771-27dc-47fb-9a42-c3374930626b"
        }
    ],
    "maskSpriteId": "1bd68408-282e-49ba-b5fe-c4ad82de252d",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "019083de-2df6-4ac8-a987-c3d890e63f16",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "10",
            "varName": "hp",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "1bd68408-282e-49ba-b5fe-c4ad82de252d",
    "visible": true
}