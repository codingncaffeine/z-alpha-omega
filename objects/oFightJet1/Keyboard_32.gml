/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 17D6879A
/// @DnDArgument : "code" "/// @description Alarm Set Firing Speed$(13_10)/// The higher the alarm = the bigger the delay$(13_10)$(13_10)if (alarm[0] = -1) alarm = 12;$(13_10)$(13_10)$(13_10)"
/// @description Alarm Set Firing Speed
/// The higher the alarm = the bigger the delay

if (alarm[0] = -1) alarm = 12;