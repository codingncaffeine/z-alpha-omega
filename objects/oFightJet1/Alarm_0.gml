/// @DnDAction : YoYo Games.Gamepad.If_Gamepad_Button_Pressed
/// @DnDVersion : 1.1
/// @DnDHash : 7B031289
/// @DnDArgument : "btn" "gp_face1"
var l7B031289_0 = 0;
var l7B031289_1 = gp_face1;
if(gamepad_is_connected(l7B031289_0) && gamepad_button_check_pressed(l7B031289_0, l7B031289_1))
{

}

/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 7FA42314
/// @DnDArgument : "xpos_relative" "1"
/// @DnDArgument : "ypos_relative" "1"
/// @DnDArgument : "objectid" "oLaser1"
/// @DnDSaveInfo : "objectid" "985946fd-046a-4475-a871-1a7e0d028992"
instance_create_layer(x + 0, y + 0, "Instances", oLaser1);

/// @DnDAction : YoYo Games.Audio.Play_Audio
/// @DnDVersion : 1
/// @DnDHash : 4157B2F3
/// @DnDArgument : "soundid" "snLasershot"
/// @DnDSaveInfo : "soundid" "107eb352-d96a-43e4-982a-fcd6944bdcb5"
audio_play_sound(snLasershot, 0, 0);