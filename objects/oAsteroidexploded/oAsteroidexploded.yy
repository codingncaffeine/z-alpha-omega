{
    "id": "6cf44b5d-a62b-4efa-b8ac-4c19a5efc6c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroidexploded",
    "eventList": [
        {
            "id": "f7e3b989-24a1-4ad0-a960-902b5c88b9be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6cf44b5d-a62b-4efa-b8ac-4c19a5efc6c2"
        },
        {
            "id": "8b1c803e-1b3d-4336-9cde-dc6fa4c1a6be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6cf44b5d-a62b-4efa-b8ac-4c19a5efc6c2"
        },
        {
            "id": "6ec6f47e-ac30-4353-b15b-2545187727eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "6cf44b5d-a62b-4efa-b8ac-4c19a5efc6c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
    "visible": true
}