/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 47FD54BF
/// @DnDArgument : "speed" "-2"
/// @DnDArgument : "speed_relative" "1"
speed += -2;

/// @DnDAction : YoYo Games.Instances.Sprite_Rotate
/// @DnDVersion : 1
/// @DnDHash : 7406915E
/// @DnDArgument : "angle" "-3"
/// @DnDArgument : "angle_relative" "1"
image_angle += -3;

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 3D529A73
/// @DnDArgument : "code" "done = 0;$(13_10)hsp = 0;$(13_10)vsp = 0;$(13_10)imagespeed = 4;$(13_10)"
done = 0;
hsp = 0;
vsp = 0;
imagespeed = 4;