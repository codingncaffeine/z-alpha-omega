{
    "id": "fbb88a76-6e7a-49bf-85f8-949bd03c4902",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemydead",
    "eventList": [
        {
            "id": "36906fa0-a92b-4be6-93dc-b02d59916b65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fbb88a76-6e7a-49bf-85f8-949bd03c4902"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
    "visible": true
}