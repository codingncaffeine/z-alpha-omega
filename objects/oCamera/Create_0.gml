/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 74502753
/// @DnDArgument : "code" "/// @description setup camera$(13_10)cam = view_camera[0];$(13_10)follow = oFightJet1;$(13_10)view_w_half = camera_get_view_width(cam) * 0.5;$(13_10)view_h_half = camera_get_view_height(cam) * 0.5;$(13_10)xTo = xstart;$(13_10)yTo = ystart;$(13_10)"
/// @description setup camera
cam = view_camera[0];
follow = oFightJet1;
view_w_half = camera_get_view_width(cam) * 0.5;
view_h_half = camera_get_view_height(cam) * 0.5;
xTo = xstart;
yTo = ystart;