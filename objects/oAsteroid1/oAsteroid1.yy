{
    "id": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAsteroid1",
    "eventList": [
        {
            "id": "fb9078a5-83f9-4913-b76f-81c16eb73a0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab"
        },
        {
            "id": "f415c1f9-6cab-4dea-a878-d7d7115821c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab"
        },
        {
            "id": "096ff64a-98c3-49ed-9e53-cdacb790bf98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab"
        },
        {
            "id": "1924a84e-5a4f-4934-ba81-4e89ae0d8e1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "5ffe09af-3d06-46a2-8ce0-ed8828a2c6ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
    "visible": true
}