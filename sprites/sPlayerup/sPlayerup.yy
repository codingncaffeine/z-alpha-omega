{
    "id": "1317cd0f-2c51-478c-ae92-f143e2af3423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b523875-8700-45be-9ae0-c8bae5aadaf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1317cd0f-2c51-478c-ae92-f143e2af3423",
            "compositeImage": {
                "id": "bb3dd29b-6a50-4203-9d95-d2a5a0412852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b523875-8700-45be-9ae0-c8bae5aadaf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3714f8e-3584-4b51-99c2-ea3c6e09fb80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b523875-8700-45be-9ae0-c8bae5aadaf9",
                    "LayerId": "8cb3139d-94e3-43a4-b8a0-b0037f0e990a"
                }
            ]
        },
        {
            "id": "28249fa1-1085-4571-98b0-bfaac340200e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1317cd0f-2c51-478c-ae92-f143e2af3423",
            "compositeImage": {
                "id": "24eff030-b4da-4ffa-b4b8-127cda5f16d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28249fa1-1085-4571-98b0-bfaac340200e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8b9eb7-4d7b-4270-bb88-3b149498caf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28249fa1-1085-4571-98b0-bfaac340200e",
                    "LayerId": "8cb3139d-94e3-43a4-b8a0-b0037f0e990a"
                }
            ]
        },
        {
            "id": "d085ade4-975f-467a-a745-1c9afb3a5beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1317cd0f-2c51-478c-ae92-f143e2af3423",
            "compositeImage": {
                "id": "bc61fec4-6160-49da-9c0d-96877c4a71d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d085ade4-975f-467a-a745-1c9afb3a5beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1b5cf9-735f-4705-8594-660f7ffbbf3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d085ade4-975f-467a-a745-1c9afb3a5beb",
                    "LayerId": "8cb3139d-94e3-43a4-b8a0-b0037f0e990a"
                }
            ]
        },
        {
            "id": "093fcb03-69b3-4d33-b938-a9e757857504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1317cd0f-2c51-478c-ae92-f143e2af3423",
            "compositeImage": {
                "id": "7b5d0bf0-a797-4580-99a0-7e0370bcb4b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093fcb03-69b3-4d33-b938-a9e757857504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "669a633e-3892-483a-871f-566dd1b59806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093fcb03-69b3-4d33-b938-a9e757857504",
                    "LayerId": "8cb3139d-94e3-43a4-b8a0-b0037f0e990a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8cb3139d-94e3-43a4-b8a0-b0037f0e990a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1317cd0f-2c51-478c-ae92-f143e2af3423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}