{
    "id": "cdc3ff15-7f1f-4f9f-801a-cc1a08674f42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sLaser1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 4,
    "bbox_right": 24,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0d17ae1-c5c6-434e-bc4f-a87b5a282044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdc3ff15-7f1f-4f9f-801a-cc1a08674f42",
            "compositeImage": {
                "id": "a803c0f5-7177-453d-9901-93ea8c37aafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d17ae1-c5c6-434e-bc4f-a87b5a282044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5014f15-d164-4047-9130-220b09906a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d17ae1-c5c6-434e-bc4f-a87b5a282044",
                    "LayerId": "0f590961-ef0e-4237-a915-d2026971e05f"
                }
            ]
        },
        {
            "id": "b1313463-4bb7-466b-bfe9-c5ce866f5cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdc3ff15-7f1f-4f9f-801a-cc1a08674f42",
            "compositeImage": {
                "id": "3d47b77f-37a8-4ac2-bf90-68118f20705b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1313463-4bb7-466b-bfe9-c5ce866f5cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e91b6bd-267f-4b2e-b01d-e45c98e5a2a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1313463-4bb7-466b-bfe9-c5ce866f5cdd",
                    "LayerId": "0f590961-ef0e-4237-a915-d2026971e05f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "0f590961-ef0e-4237-a915-d2026971e05f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdc3ff15-7f1f-4f9f-801a-cc1a08674f42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 14,
    "yorig": 14
}