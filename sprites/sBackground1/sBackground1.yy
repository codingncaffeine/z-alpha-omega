{
    "id": "2ffdfa59-6094-40d0-99ea-44e6518152aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1919,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3315d0f-b6ac-409a-8e25-047a29dfe47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ffdfa59-6094-40d0-99ea-44e6518152aa",
            "compositeImage": {
                "id": "96589f73-3b26-4451-a684-bb1d29874d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3315d0f-b6ac-409a-8e25-047a29dfe47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473538f4-bde9-4478-b098-7832e011af0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3315d0f-b6ac-409a-8e25-047a29dfe47a",
                    "LayerId": "40d19406-3294-4544-a1b2-dbd8a03becf4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1920,
    "layers": [
        {
            "id": "40d19406-3294-4544-a1b2-dbd8a03becf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ffdfa59-6094-40d0-99ea-44e6518152aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}