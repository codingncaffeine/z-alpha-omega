{
    "id": "1fdaa46d-0806-4563-ba6e-d4f450a6bf74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitleBackground",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 1599,
    "bbox_left": 0,
    "bbox_right": 1599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a20d2b1-a457-4e6c-8402-ce95ca8a0800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1fdaa46d-0806-4563-ba6e-d4f450a6bf74",
            "compositeImage": {
                "id": "badfb0cc-9433-4e6b-a486-3402849b3ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a20d2b1-a457-4e6c-8402-ce95ca8a0800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c0092a-cebc-4d26-9edc-fdb22d85b59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a20d2b1-a457-4e6c-8402-ce95ca8a0800",
                    "LayerId": "1778be88-0897-4999-b27c-f144fed55208"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1600,
    "layers": [
        {
            "id": "1778be88-0897-4999-b27c-f144fed55208",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1fdaa46d-0806-4563-ba6e-d4f450a6bf74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}