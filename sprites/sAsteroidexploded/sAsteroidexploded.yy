{
    "id": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroidexploded",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 29,
    "bbox_right": 83,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc8b7608-8060-462e-9dc0-d1a3b6fbf2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
            "compositeImage": {
                "id": "00dec5c1-64f4-4045-a143-c43c2a993629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc8b7608-8060-462e-9dc0-d1a3b6fbf2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cef02a7-d927-448e-8864-6fa296a6f800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc8b7608-8060-462e-9dc0-d1a3b6fbf2c7",
                    "LayerId": "e1cc3b87-c969-4286-8ea0-3af87a0382c3"
                }
            ]
        },
        {
            "id": "be49fbdb-1ab0-49e3-9e79-ca31d2ba89df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
            "compositeImage": {
                "id": "a73cbdf2-3041-477f-b255-8cad0c26b6f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be49fbdb-1ab0-49e3-9e79-ca31d2ba89df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5784e49-e3f1-434d-ba00-3f51bee3d8a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be49fbdb-1ab0-49e3-9e79-ca31d2ba89df",
                    "LayerId": "e1cc3b87-c969-4286-8ea0-3af87a0382c3"
                }
            ]
        },
        {
            "id": "6916e746-cd4f-410f-abc4-28193f601fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
            "compositeImage": {
                "id": "fd78979f-70e3-461d-b690-f831b5e29462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6916e746-cd4f-410f-abc4-28193f601fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53989245-d39f-46f7-b04d-1105c7ee8e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6916e746-cd4f-410f-abc4-28193f601fc7",
                    "LayerId": "e1cc3b87-c969-4286-8ea0-3af87a0382c3"
                }
            ]
        },
        {
            "id": "4e700198-3e8c-4a7e-8e0a-22e757c41cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
            "compositeImage": {
                "id": "973f5058-464e-4c55-bfb2-97cd4d1fb517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e700198-3e8c-4a7e-8e0a-22e757c41cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f841cac-e9d6-4945-bb43-a268e3a15b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e700198-3e8c-4a7e-8e0a-22e757c41cbc",
                    "LayerId": "e1cc3b87-c969-4286-8ea0-3af87a0382c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 92,
    "layers": [
        {
            "id": "e1cc3b87-c969-4286-8ea0-3af87a0382c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b15e1038-e5a9-4cf4-a864-2f0da1b94a6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 0,
    "yorig": 0
}