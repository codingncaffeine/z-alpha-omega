{
    "id": "5896ff54-9111-4a82-8945-2ba6a61d0296",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 0,
    "bbox_right": 97,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "701d28f5-8f6f-4965-9643-382bef2db9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5896ff54-9111-4a82-8945-2ba6a61d0296",
            "compositeImage": {
                "id": "426da69e-80c4-484b-b4d9-490abea5491d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "701d28f5-8f6f-4965-9643-382bef2db9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19fb8de9-9697-4e6e-a04c-e1375a7d9358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "701d28f5-8f6f-4965-9643-382bef2db9e7",
                    "LayerId": "0dededc3-94af-4c8f-af85-5429c74ab558"
                }
            ]
        },
        {
            "id": "a7ae3c24-5a46-4e42-a340-98d7a152d203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5896ff54-9111-4a82-8945-2ba6a61d0296",
            "compositeImage": {
                "id": "9eea7afc-3830-4525-85a7-e5f4e4c04955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ae3c24-5a46-4e42-a340-98d7a152d203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c17be04-2b13-471a-8b76-9cf2180a6198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ae3c24-5a46-4e42-a340-98d7a152d203",
                    "LayerId": "0dededc3-94af-4c8f-af85-5429c74ab558"
                }
            ]
        },
        {
            "id": "afe05467-be4f-47c8-9d53-213796706676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5896ff54-9111-4a82-8945-2ba6a61d0296",
            "compositeImage": {
                "id": "a68d9bc2-668d-4786-81f2-51c363f9d148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe05467-be4f-47c8-9d53-213796706676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4870025f-b225-45cd-a8de-aed98e7cd030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe05467-be4f-47c8-9d53-213796706676",
                    "LayerId": "0dededc3-94af-4c8f-af85-5429c74ab558"
                }
            ]
        },
        {
            "id": "e3b0b41c-25e3-45ac-83a0-5fd3baba3bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5896ff54-9111-4a82-8945-2ba6a61d0296",
            "compositeImage": {
                "id": "92355b53-aab4-4516-9d76-edec104bf51a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b0b41c-25e3-45ac-83a0-5fd3baba3bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bccc1429-9b8f-481f-b2e9-49de27731b4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b0b41c-25e3-45ac-83a0-5fd3baba3bee",
                    "LayerId": "0dededc3-94af-4c8f-af85-5429c74ab558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "0dededc3-94af-4c8f-af85-5429c74ab558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5896ff54-9111-4a82-8945-2ba6a61d0296",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}