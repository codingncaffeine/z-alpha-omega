{
    "id": "b88500d9-f713-4e03-9a2c-e8368659c70b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExhaust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f472ac4-81f1-49ce-8efd-5090d894b3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b88500d9-f713-4e03-9a2c-e8368659c70b",
            "compositeImage": {
                "id": "79ba1eb9-beee-4a7b-9027-e6c8018651ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f472ac4-81f1-49ce-8efd-5090d894b3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed638590-e652-46ff-9e35-2109f38952e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f472ac4-81f1-49ce-8efd-5090d894b3d0",
                    "LayerId": "31231ef1-ec53-4382-ba87-0910aa6e60c4"
                }
            ]
        },
        {
            "id": "4d245b23-d27f-403b-aca7-3f6b1f007d5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b88500d9-f713-4e03-9a2c-e8368659c70b",
            "compositeImage": {
                "id": "548f21a5-4241-47e8-9b00-d29e10962836",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d245b23-d27f-403b-aca7-3f6b1f007d5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce041692-ba37-4eb2-ae04-6196be922030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d245b23-d27f-403b-aca7-3f6b1f007d5a",
                    "LayerId": "31231ef1-ec53-4382-ba87-0910aa6e60c4"
                }
            ]
        },
        {
            "id": "1251255b-621a-4e51-bb95-c825b0982730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b88500d9-f713-4e03-9a2c-e8368659c70b",
            "compositeImage": {
                "id": "a899ef35-73b4-4a8a-8d55-3611f59aa4a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1251255b-621a-4e51-bb95-c825b0982730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec6fb08-313d-4335-8de0-890504615117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1251255b-621a-4e51-bb95-c825b0982730",
                    "LayerId": "31231ef1-ec53-4382-ba87-0910aa6e60c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "31231ef1-ec53-4382-ba87-0910aa6e60c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b88500d9-f713-4e03-9a2c-e8368659c70b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -163,
    "yorig": -20
}