{
    "id": "48ee59ee-3b72-4d06-a3ee-6d75f26862c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemyfighter1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a56c344e-e426-4312-9441-04cbd17d6bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48ee59ee-3b72-4d06-a3ee-6d75f26862c1",
            "compositeImage": {
                "id": "230ffa2c-d613-428a-8800-730e667c93f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56c344e-e426-4312-9441-04cbd17d6bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5341bcee-3bcc-406d-99b4-a7a9d701ae28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56c344e-e426-4312-9441-04cbd17d6bb8",
                    "LayerId": "374b3988-c616-46b7-a6dc-135298103cba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "374b3988-c616-46b7-a6dc-135298103cba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48ee59ee-3b72-4d06-a3ee-6d75f26862c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 51,
    "xorig": 0,
    "yorig": 0
}