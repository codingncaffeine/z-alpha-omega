{
    "id": "95f65af2-485f-4a70-8dc6-5b048999d1fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground6",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 2557,
    "bbox_left": 0,
    "bbox_right": 2559,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282ae131-bcb0-4e2c-b286-918909f4d2ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95f65af2-485f-4a70-8dc6-5b048999d1fd",
            "compositeImage": {
                "id": "a53a3054-4790-43cd-93d5-9ffd92912089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282ae131-bcb0-4e2c-b286-918909f4d2ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1892c395-304a-47e0-80a8-128f81fce609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282ae131-bcb0-4e2c-b286-918909f4d2ea",
                    "LayerId": "9f209926-caa5-4eef-a7d7-9052cf743bf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2560,
    "layers": [
        {
            "id": "9f209926-caa5-4eef-a7d7-9052cf743bf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95f65af2-485f-4a70-8dc6-5b048999d1fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}