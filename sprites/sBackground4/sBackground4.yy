{
    "id": "7654b4fc-aae0-4856-8b34-9cf473c93345",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground4",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 2559,
    "bbox_left": 2,
    "bbox_right": 2559,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b4cd1ce-d81e-407c-aa24-7e10c04e96fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7654b4fc-aae0-4856-8b34-9cf473c93345",
            "compositeImage": {
                "id": "8c49288c-a39d-4618-be7c-f340f3497665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b4cd1ce-d81e-407c-aa24-7e10c04e96fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e662f91e-8a35-44ce-a566-fdbe721d46b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b4cd1ce-d81e-407c-aa24-7e10c04e96fd",
                    "LayerId": "657049b5-ed90-45c1-8f10-33b461d2af17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2560,
    "layers": [
        {
            "id": "657049b5-ed90-45c1-8f10-33b461d2af17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7654b4fc-aae0-4856-8b34-9cf473c93345",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2560,
    "xorig": 0,
    "yorig": 0
}