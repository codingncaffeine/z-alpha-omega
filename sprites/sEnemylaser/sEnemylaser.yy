{
    "id": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemylaser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 0,
    "bbox_right": 94,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e26fe262-6aa4-415f-8e8d-b9c3ddb5d8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
            "compositeImage": {
                "id": "692c2c45-7f58-41d9-b466-1574fae40c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26fe262-6aa4-415f-8e8d-b9c3ddb5d8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3b404e-2018-4120-9a0e-d3f70556374a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26fe262-6aa4-415f-8e8d-b9c3ddb5d8b3",
                    "LayerId": "f1e03040-f923-40fe-87ea-075c926d68ab"
                }
            ]
        },
        {
            "id": "bc800224-48cd-420e-8cf8-a7def0c512cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
            "compositeImage": {
                "id": "9d348982-2b8a-47a5-b218-1cb6cc29c2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc800224-48cd-420e-8cf8-a7def0c512cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558f8a27-f049-466f-9751-91414d54b1f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc800224-48cd-420e-8cf8-a7def0c512cf",
                    "LayerId": "f1e03040-f923-40fe-87ea-075c926d68ab"
                }
            ]
        },
        {
            "id": "c5a9fa36-7a0c-4b1b-9090-60622b0046bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
            "compositeImage": {
                "id": "9b6148a3-9322-470b-8d0c-3412f5823c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a9fa36-7a0c-4b1b-9090-60622b0046bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e948b356-f259-4075-8da6-3af86816ffdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a9fa36-7a0c-4b1b-9090-60622b0046bb",
                    "LayerId": "f1e03040-f923-40fe-87ea-075c926d68ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 67,
    "layers": [
        {
            "id": "f1e03040-f923-40fe-87ea-075c926d68ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf6dc629-0d93-46d1-9bf9-f638f51d1b81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 0,
    "yorig": 0
}