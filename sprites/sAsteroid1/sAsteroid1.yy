{
    "id": "7a606f06-394e-4f9d-a372-9bde440c6476",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sAsteroid1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 12,
    "bbox_right": 106,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0675218-41dd-4e81-b634-8420313013a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "b3246492-bf8d-4cc8-b850-12dfd097e43f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0675218-41dd-4e81-b634-8420313013a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67af6371-b803-488a-bc7c-f1112bd2d102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0675218-41dd-4e81-b634-8420313013a3",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "a0df6503-330b-4b27-a981-43be3d56a4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "2d8fe114-99db-452f-a671-ed76bbb1b650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0df6503-330b-4b27-a981-43be3d56a4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0ed770d-73aa-4791-98fa-1219a280aae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0df6503-330b-4b27-a981-43be3d56a4da",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "765496dd-5fda-43d3-acb2-47dacd5ea07f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "23f0800b-e16e-48e5-b2fe-137b591009d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765496dd-5fda-43d3-acb2-47dacd5ea07f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232725a8-2a59-47e8-bc59-ed3a67bb6634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765496dd-5fda-43d3-acb2-47dacd5ea07f",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "34df3619-6e98-46fa-ba1d-c98f4c47aca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "628bd09d-5de7-48a7-96bd-fd6032d15fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34df3619-6e98-46fa-ba1d-c98f4c47aca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98e6fbe-317a-445f-900f-2d691328e5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34df3619-6e98-46fa-ba1d-c98f4c47aca2",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "14ff03f4-020f-4c53-ad53-6aefcddb1402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "99aedefa-fd01-45c2-84a3-3f7263b04305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ff03f4-020f-4c53-ad53-6aefcddb1402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5bfa2d7-1bea-46fb-8e39-cd35acacfa11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ff03f4-020f-4c53-ad53-6aefcddb1402",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "a1eef503-9a8c-42bc-8901-ebd11b4a1571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "92296d7b-aabd-4f0f-aee3-2356f55b2223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1eef503-9a8c-42bc-8901-ebd11b4a1571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e283bc41-bef3-46a3-b702-6498eb33239a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1eef503-9a8c-42bc-8901-ebd11b4a1571",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "cfa0e55b-4ba8-4bde-97ab-3cd95c76846e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "fc7082c5-45a8-4581-bbd0-91a1b7afe635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfa0e55b-4ba8-4bde-97ab-3cd95c76846e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a55d57-e3a2-47cf-b57d-c47ff023512e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfa0e55b-4ba8-4bde-97ab-3cd95c76846e",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "9a605e5c-fe07-4c07-bffc-101abab85f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "5f263248-62bc-4300-8bd5-59b1accc5e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a605e5c-fe07-4c07-bffc-101abab85f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c50a10e-28fa-4e62-8ccb-47dbc2cd21c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a605e5c-fe07-4c07-bffc-101abab85f50",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "97c5626c-bc6e-4098-9a87-ac0d61017231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "da18e306-e0c5-453f-81ab-8ee814970a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97c5626c-bc6e-4098-9a87-ac0d61017231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8c48da-56a6-4822-a9b8-a323b9616825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97c5626c-bc6e-4098-9a87-ac0d61017231",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "4022ba1c-94ff-4990-8451-c88d73bcf526",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "7b627369-acfa-4284-bf37-56e43044b42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4022ba1c-94ff-4990-8451-c88d73bcf526",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9c0d50e-60e0-44e0-a12f-3504aa9194fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4022ba1c-94ff-4990-8451-c88d73bcf526",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "04b3d139-9246-432c-8a83-b8e01562f438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "6684e15f-4242-46bc-b0cc-9598de501cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b3d139-9246-432c-8a83-b8e01562f438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc00543f-2a91-4c56-8bbc-93702df0ee18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b3d139-9246-432c-8a83-b8e01562f438",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "da9e8cca-ab2d-477e-9660-c10830530a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "e22fc30d-885e-403c-8aed-66c788640dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9e8cca-ab2d-477e-9660-c10830530a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b87f125-9832-4cfb-b25d-1b175b7c6f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9e8cca-ab2d-477e-9660-c10830530a95",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "39468965-0c92-40e6-9db6-0735ed9bec6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "62dbd278-2bae-4d01-8bce-77f6006c95b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39468965-0c92-40e6-9db6-0735ed9bec6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129dcaa7-77ab-4d94-9046-d3fa9ce3a994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39468965-0c92-40e6-9db6-0735ed9bec6e",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "a71ce10a-da86-4882-a8a1-d07ae205ac2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "7cf99964-4909-44d1-bdc1-127a22194e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71ce10a-da86-4882-a8a1-d07ae205ac2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a91268bc-263b-4f52-a417-68be16b4063b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71ce10a-da86-4882-a8a1-d07ae205ac2d",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "bc5245a1-74d3-445b-bb49-529eef222d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "fc24e3d5-db98-451d-a91e-ddddaafc9d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5245a1-74d3-445b-bb49-529eef222d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94230065-17d6-45b1-a754-60f471b180ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5245a1-74d3-445b-bb49-529eef222d3c",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "a3f06478-5f81-4e6f-9fe9-78c0e6c61c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "e53101ce-8510-4feb-b542-7e204f804bf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f06478-5f81-4e6f-9fe9-78c0e6c61c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f6eff0c-e4c7-4614-92ed-41936b78ccbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f06478-5f81-4e6f-9fe9-78c0e6c61c7a",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "88036ec8-e0ea-4f4e-8457-2a6bf7a67ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "2135c779-554f-47e4-ab50-329e7f30d461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88036ec8-e0ea-4f4e-8457-2a6bf7a67ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97883c16-2191-4781-b573-00f664846411",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88036ec8-e0ea-4f4e-8457-2a6bf7a67ab8",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "2efa2e14-6b28-4032-9c34-bf074877f2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "65409918-4e6f-4732-9a14-24e95089a54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2efa2e14-6b28-4032-9c34-bf074877f2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb89a23c-4b0b-4165-9ffa-c72f4f536562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2efa2e14-6b28-4032-9c34-bf074877f2c7",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "5ece3d5f-4c08-4bea-8415-50fea7ef63f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "10b729c5-2a16-4950-b383-8cfff3013cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ece3d5f-4c08-4bea-8415-50fea7ef63f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc74bc54-34e9-4ae5-bad4-07077400a7cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ece3d5f-4c08-4bea-8415-50fea7ef63f7",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "2d36b84b-e301-4827-8c07-a516fd51c863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "eb908800-6fab-4342-90dc-eca03423ab8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d36b84b-e301-4827-8c07-a516fd51c863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf17f96-18bc-4143-ac2e-ce6278dd8d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d36b84b-e301-4827-8c07-a516fd51c863",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "315eb3a3-a518-4df5-84f7-f225fb1867a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "e01db878-838c-4699-8072-0de1b48ebed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "315eb3a3-a518-4df5-84f7-f225fb1867a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c60132f-8147-4986-a792-a003d41cd274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "315eb3a3-a518-4df5-84f7-f225fb1867a2",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "8ff84ea2-fec2-4f76-9b16-0b913d8cf278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "d7a4fc78-cc80-41f2-821e-fef8b3cccfe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff84ea2-fec2-4f76-9b16-0b913d8cf278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2b06e91-7847-4d92-9a2a-56cc0fc132af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff84ea2-fec2-4f76-9b16-0b913d8cf278",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "04538362-1550-44ed-8bd7-7bd3cf601cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "ce8cf767-a2af-411c-9232-9b23e23ab474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04538362-1550-44ed-8bd7-7bd3cf601cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02fd91b5-5231-4e8b-8d7a-ef8f3252ceb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04538362-1550-44ed-8bd7-7bd3cf601cea",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "fbf5636a-2a56-4f90-b8d6-bce90d43cf64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "7a6ac95b-4191-476f-ab4d-4ef2f4017fab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf5636a-2a56-4f90-b8d6-bce90d43cf64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea2de82-bdb7-4f3f-b550-47f358a2c6d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf5636a-2a56-4f90-b8d6-bce90d43cf64",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "ff164cc2-f5bc-473b-94b9-a2c06c591d89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "02832695-7be3-410d-b1cf-1c8719e8bc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff164cc2-f5bc-473b-94b9-a2c06c591d89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4a0f7d-0a59-4f39-836d-84fd296f07d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff164cc2-f5bc-473b-94b9-a2c06c591d89",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "692e86aa-9c30-4d4d-b0ff-6292bbc1c1c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "7af095df-c9d6-4ccb-af91-7f24dd444a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "692e86aa-9c30-4d4d-b0ff-6292bbc1c1c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8673d2e-22b8-4e59-a871-ea68476e86af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "692e86aa-9c30-4d4d-b0ff-6292bbc1c1c3",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "10e471b0-e7b6-476a-af0e-5fbbc5217dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "00796e22-91d3-42b8-b7d9-ebc3d61ecb60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10e471b0-e7b6-476a-af0e-5fbbc5217dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5528016e-3b32-4ca7-9292-31c75facb5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10e471b0-e7b6-476a-af0e-5fbbc5217dec",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "b212d30a-039b-4081-98b2-1de6be1076d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "30452ff8-f069-4d21-98c7-a1db87e22d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b212d30a-039b-4081-98b2-1de6be1076d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6fdd33c-4189-4c42-ae03-79be25f0a312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b212d30a-039b-4081-98b2-1de6be1076d2",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "3f19098e-bfdb-43a5-ab85-7f353d1db8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "954c1802-da46-428d-bc3c-c32b01132f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f19098e-bfdb-43a5-ab85-7f353d1db8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf78b45-2521-4027-9875-8cbd66ced0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f19098e-bfdb-43a5-ab85-7f353d1db8ca",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        },
        {
            "id": "c6286032-23a7-46ef-8142-4c73749b0d18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "compositeImage": {
                "id": "168ed6d9-efd5-4d65-98f7-57c9b9fae978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6286032-23a7-46ef-8142-4c73749b0d18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8be9977-955d-4d6f-be0c-7bd730cbd452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6286032-23a7-46ef-8142-4c73749b0d18",
                    "LayerId": "38b7e50c-ec21-421f-9437-352b9448832b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 91,
    "layers": [
        {
            "id": "38b7e50c-ec21-421f-9437-352b9448832b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a606f06-394e-4f9d-a372-9bde440c6476",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 109,
    "xorig": 54,
    "yorig": 45
}