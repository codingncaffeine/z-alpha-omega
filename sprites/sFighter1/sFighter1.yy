{
    "id": "1bd68408-282e-49ba-b5fe-c4ad82de252d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFighter1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 16,
    "bbox_right": 78,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba8f7fc5-df38-403e-8445-b38131d51cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bd68408-282e-49ba-b5fe-c4ad82de252d",
            "compositeImage": {
                "id": "045f5e9b-bbd6-4ab7-aec3-73614575da38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8f7fc5-df38-403e-8445-b38131d51cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e658d6f-6181-4b04-a6ac-4ec68bb0c32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8f7fc5-df38-403e-8445-b38131d51cad",
                    "LayerId": "e6919a3f-280e-41cc-984f-1dcf1b6383fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "e6919a3f-280e-41cc-984f-1dcf1b6383fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bd68408-282e-49ba-b5fe-c4ad82de252d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}