{
    "id": "0f70f0f1-6a34-47b4-8310-0563a9d63372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBackground5",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 1999,
    "bbox_left": 0,
    "bbox_right": 1999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b27d1c95-8537-4bed-8f98-c6630f9f9d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f70f0f1-6a34-47b4-8310-0563a9d63372",
            "compositeImage": {
                "id": "31d74427-4a7f-44a7-8088-6e55301794db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b27d1c95-8537-4bed-8f98-c6630f9f9d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafab924-1156-4603-887a-2d22e44d80c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b27d1c95-8537-4bed-8f98-c6630f9f9d73",
                    "LayerId": "4e8f21a2-ed80-4a7c-9e72-3b1b7c6eb6bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2000,
    "layers": [
        {
            "id": "4e8f21a2-ed80-4a7c-9e72-3b1b7c6eb6bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f70f0f1-6a34-47b4-8310-0563a9d63372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2000,
    "xorig": 0,
    "yorig": 0
}