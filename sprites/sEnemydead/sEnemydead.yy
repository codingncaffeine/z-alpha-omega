{
    "id": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sEnemydead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 4,
    "bbox_right": 123,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41578a18-7e02-493c-b81f-27bf406e1385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "773b9394-121b-40a2-9fc5-eee5cfb500e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41578a18-7e02-493c-b81f-27bf406e1385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe42848c-a427-4508-a119-eeb1c506053c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41578a18-7e02-493c-b81f-27bf406e1385",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "29ad0506-999c-49d0-91ca-57d1046d3905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "0a815d71-e592-4e28-a78f-5511de037c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ad0506-999c-49d0-91ca-57d1046d3905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ea1a7d0-4fcf-4742-b34e-0be185db811b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ad0506-999c-49d0-91ca-57d1046d3905",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "b63d7761-80ee-40ad-9520-b05a79330a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "ad20b03e-dd8b-42c4-bae7-3ebe1650b6e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63d7761-80ee-40ad-9520-b05a79330a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56576a48-7aa6-4a5f-9bb5-177b25cfd8ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63d7761-80ee-40ad-9520-b05a79330a73",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "2d6c606e-ccc6-4baa-95dc-344e3cc96982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "992434d9-2de7-4d67-95e2-57a1a0376735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d6c606e-ccc6-4baa-95dc-344e3cc96982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7514264d-e8e8-46cb-b46a-269be019ef1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d6c606e-ccc6-4baa-95dc-344e3cc96982",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "50bee7f0-83b5-4d48-96b4-bb005edcdaa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "5ab199bf-d1ea-4c8a-ac53-4893af4ec624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50bee7f0-83b5-4d48-96b4-bb005edcdaa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da55624e-7b4e-45f7-a751-6b3f6e666fd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50bee7f0-83b5-4d48-96b4-bb005edcdaa3",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "0486520f-5ee1-438b-8b67-773bd4141d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "7a3c6264-c050-4262-af0d-319973e2c983",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0486520f-5ee1-438b-8b67-773bd4141d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "562dc7d8-ce48-46e9-9efa-18b4039a4bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0486520f-5ee1-438b-8b67-773bd4141d2f",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "7661b6d8-15fb-415a-a520-263dab6e71bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "84ea865e-a3ba-4e8a-bfe0-fc879e4b70bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7661b6d8-15fb-415a-a520-263dab6e71bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f727a88-8f60-4b2c-9769-e0a8ca8eebca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7661b6d8-15fb-415a-a520-263dab6e71bb",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "339aa254-62e6-4121-8c77-078bb87c867d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "5e81d27e-33ce-4618-a0ab-a07f56ee22a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "339aa254-62e6-4121-8c77-078bb87c867d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541a3402-ffb9-4b0e-ab88-de6af9576978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "339aa254-62e6-4121-8c77-078bb87c867d",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "54ee7bb9-5535-4326-adbc-e39c53fe2f5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "a78ba814-c580-43a4-b4e3-e3f7db4c823a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ee7bb9-5535-4326-adbc-e39c53fe2f5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f062df6-4783-4a76-91f9-83f3a51cb92c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ee7bb9-5535-4326-adbc-e39c53fe2f5e",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        },
        {
            "id": "8387923f-84c1-4574-aa78-6e9a02bd0dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "compositeImage": {
                "id": "65adb6ab-ae5f-4539-966c-a135a300358d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8387923f-84c1-4574-aa78-6e9a02bd0dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6ecddc-4e57-4117-b1fe-e5b26df6ec1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8387923f-84c1-4574-aa78-6e9a02bd0dbe",
                    "LayerId": "ff61ef68-d085-4c4f-8464-0912d797a78a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ff61ef68-d085-4c4f-8464-0912d797a78a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9280dd06-0b87-481f-a2ca-fa69f22aaf3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}