{
    "id": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 408,
    "bbox_left": 412,
    "bbox_right": 998,
    "bbox_top": 179,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10460351-0732-418d-8162-558080eeef16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "0d625343-74db-4be1-af31-ecaa45a72234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10460351-0732-418d-8162-558080eeef16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e6cc3e-034d-4c68-9382-026fb94b06e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10460351-0732-418d-8162-558080eeef16",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "f707703b-5fc1-4a83-98fc-21adec30ae10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "c8c46c04-7cb3-4392-bd05-d96ea581a431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f707703b-5fc1-4a83-98fc-21adec30ae10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4be32a4-dd18-4c19-ba07-c3feb5a9f8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f707703b-5fc1-4a83-98fc-21adec30ae10",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "983d8be9-a80a-42bd-9523-b7246fb26fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "1fb3c7cb-16ad-4b33-b7ed-53b473d58423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "983d8be9-a80a-42bd-9523-b7246fb26fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce82f60-3bf9-4cc7-baf3-b5e05f468915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "983d8be9-a80a-42bd-9523-b7246fb26fce",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "a7706321-c220-4a09-9f6c-a4a5143992a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "5239e469-c84f-4037-9854-3b3067f70d42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7706321-c220-4a09-9f6c-a4a5143992a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "652315c9-625b-46ee-9d1a-ffcbff7ef3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7706321-c220-4a09-9f6c-a4a5143992a7",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "6a9eff93-475a-4bcd-924f-2b90cc38b36f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "e13fb4f3-9e4c-4422-92bf-b9c4a3e1bcd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9eff93-475a-4bcd-924f-2b90cc38b36f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc11020-e077-44ed-ace3-6e63a5392148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9eff93-475a-4bcd-924f-2b90cc38b36f",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "397819af-d864-44ec-9107-cdbcd3a10f7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "9805f764-1b9d-46a5-a2d1-959dba5acf23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "397819af-d864-44ec-9107-cdbcd3a10f7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1834a7-3726-487f-ade2-5c5ed082d146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "397819af-d864-44ec-9107-cdbcd3a10f7d",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "1ce83b73-d96f-47b8-9c53-e553a12c52b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "a14325c8-13da-4177-8302-2d3d87be07bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ce83b73-d96f-47b8-9c53-e553a12c52b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4bc62f-7a62-4ba7-bc84-9a7209804873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ce83b73-d96f-47b8-9c53-e553a12c52b2",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "1da8f568-784c-4415-ab84-07bfe47863e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "0ae76fa4-fcc1-4f33-b45f-2095ac844a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da8f568-784c-4415-ab84-07bfe47863e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9895af3c-92fc-4eb9-af8f-535a453fae06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da8f568-784c-4415-ab84-07bfe47863e9",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "f945c082-6bfc-45c9-8b95-e700bb9d2e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "c6bcf605-9fbf-426a-bb7a-6d9fb32d4780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f945c082-6bfc-45c9-8b95-e700bb9d2e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f9c7945-4a05-4272-94df-ab6d2f4da113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f945c082-6bfc-45c9-8b95-e700bb9d2e97",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "a1284885-c88b-415a-9e9a-0ba067aa3fb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "cad1a2b8-8a8b-4cf1-a719-c3be3fa0fb3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1284885-c88b-415a-9e9a-0ba067aa3fb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7124ad25-d104-4b70-ae91-f161aa9c25ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1284885-c88b-415a-9e9a-0ba067aa3fb4",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "d1b1f85a-ae26-488b-a9e9-1e4b6cb6d97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "a5a19e17-ec0f-40b2-8330-e059307ff2c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1b1f85a-ae26-488b-a9e9-1e4b6cb6d97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7c9a43-6eef-44d3-8ff8-a97805120480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1b1f85a-ae26-488b-a9e9-1e4b6cb6d97d",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "2e686df8-e2e7-43ad-a028-b3d7608a0956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "2410f926-996c-47fb-a53a-30f113f6b253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e686df8-e2e7-43ad-a028-b3d7608a0956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a19fd2-d5b8-41d5-9ad9-28a286ce424e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e686df8-e2e7-43ad-a028-b3d7608a0956",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "b4f92e34-f2ba-4759-a0eb-b60a69397a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "3b5e399f-0fd4-42f2-b0ad-fdfd6ac9c132",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f92e34-f2ba-4759-a0eb-b60a69397a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d09094-37a3-4c60-9f83-3feaec6494cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f92e34-f2ba-4759-a0eb-b60a69397a66",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "3d676185-cbc4-4287-a3c1-561f0e833757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "c62964e6-5f41-4a2f-bcec-34c4abbb7f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d676185-cbc4-4287-a3c1-561f0e833757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19d897fb-e2a9-4d21-9a9b-31d299798d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d676185-cbc4-4287-a3c1-561f0e833757",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "e7bc94c3-6eef-4b32-baf9-9f83e5530e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "dc15109d-0c2a-4ed8-98e9-e6d0bf8d5ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bc94c3-6eef-4b32-baf9-9f83e5530e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18c0d4c-6194-4f93-a1f8-459db1f66140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bc94c3-6eef-4b32-baf9-9f83e5530e12",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "7b3aeb5b-5979-4039-a71b-c16d9a52a547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "b3d058d4-ff00-4170-99d8-ce795dd91dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3aeb5b-5979-4039-a71b-c16d9a52a547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ffe020-2457-45d7-b3a2-0d3116a8bad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3aeb5b-5979-4039-a71b-c16d9a52a547",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "a1bc43b0-7e97-4440-88fc-70dff90547f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "cb2636f9-e76d-4a7c-a64c-0eb777f0fbf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1bc43b0-7e97-4440-88fc-70dff90547f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc8e5d7-b002-4500-9f4a-6e9966c440c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1bc43b0-7e97-4440-88fc-70dff90547f9",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "95d3e005-0f45-4aa9-a7b0-c78bca2501b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "8cecc817-0a50-45bc-993f-187d4f4cbc97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d3e005-0f45-4aa9-a7b0-c78bca2501b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa53c31-4e67-4aa9-a235-a9aab9ff1b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d3e005-0f45-4aa9-a7b0-c78bca2501b6",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "0d1e2aca-e889-4744-a2ff-6a6e8ac47f81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "1856612e-01fa-4404-8c73-afce505b2c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1e2aca-e889-4744-a2ff-6a6e8ac47f81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367e5e32-62d0-4287-80ec-d9f5f621d3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1e2aca-e889-4744-a2ff-6a6e8ac47f81",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "48ef84cf-2283-4cbb-9593-e198601ba9c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "04811435-34f1-4af7-b8d0-b8da959f3213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ef84cf-2283-4cbb-9593-e198601ba9c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b5d64da-d05a-4709-b794-d0a6bb526085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ef84cf-2283-4cbb-9593-e198601ba9c0",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "8a4415fd-9f4f-4a40-83a9-7b5080e863d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "6934c012-777e-4a1f-bd5e-e124d25fc28f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4415fd-9f4f-4a40-83a9-7b5080e863d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238d9820-ecff-492f-be28-e565ab324b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4415fd-9f4f-4a40-83a9-7b5080e863d9",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "131e729c-7e2f-4f77-9920-db50b2deb622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "a8e191eb-9eca-4bbe-980b-13f9fff24cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131e729c-7e2f-4f77-9920-db50b2deb622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18cf2002-2c06-427d-b25c-e91e4695a183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131e729c-7e2f-4f77-9920-db50b2deb622",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "7b662082-03c4-474b-9987-918fc6b49fd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "a221b083-c7a3-4509-bc70-4787c391b142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b662082-03c4-474b-9987-918fc6b49fd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "660b54c5-3f32-4dc1-87e4-f82069feb064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b662082-03c4-474b-9987-918fc6b49fd4",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "69f85404-365d-476e-a3c4-ab24b34a85fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "f6f8165b-8e5a-4836-bd8e-43be6299ce9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f85404-365d-476e-a3c4-ab24b34a85fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8118fd-cb8f-41de-bcbe-2b40011e0a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f85404-365d-476e-a3c4-ab24b34a85fb",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "0ea5bde1-2d4a-4223-bfa9-a8b192f6ac10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "edefabac-96c8-42b5-807d-6309e163cbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea5bde1-2d4a-4223-bfa9-a8b192f6ac10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebceb77c-2820-4290-bcb3-9b52ee2b6417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea5bde1-2d4a-4223-bfa9-a8b192f6ac10",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "faea9dd1-f0da-4249-bf6b-9a4ad1b15c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "450e30c3-1095-4635-be14-1b19d261dc1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faea9dd1-f0da-4249-bf6b-9a4ad1b15c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76969529-733d-406f-9aff-16707d221530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faea9dd1-f0da-4249-bf6b-9a4ad1b15c48",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "5b41f722-87be-4495-8bd2-548b102f3725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "8ee11821-b54e-48bb-bb9f-840e4c96b751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b41f722-87be-4495-8bd2-548b102f3725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5605063-082d-49d8-b540-1b0e4deff722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b41f722-87be-4495-8bd2-548b102f3725",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "4fe4aaa4-13fe-4e35-a46f-75b321583a0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "64cf40e2-2426-444e-b445-30b6aad6f73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe4aaa4-13fe-4e35-a46f-75b321583a0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "641c6663-a38d-4f2b-ad21-15b9532a0464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe4aaa4-13fe-4e35-a46f-75b321583a0b",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "aa241f25-88c6-4425-ae9f-f396cb791d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "05c2c386-56f9-4a90-b360-6cf8424b4129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa241f25-88c6-4425-ae9f-f396cb791d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5abcdd64-0129-4e9a-aa11-8e04e5e1a59d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa241f25-88c6-4425-ae9f-f396cb791d41",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "4d31eee0-8e95-4c2a-98c1-b44905d5ec20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "670771dc-ba02-495a-9a41-02cad55714bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d31eee0-8e95-4c2a-98c1-b44905d5ec20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5038c4f-b5fb-4189-9744-2f38e2ad3333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d31eee0-8e95-4c2a-98c1-b44905d5ec20",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        },
        {
            "id": "6ad6623a-75b0-44e9-8981-364a7b723c67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "compositeImage": {
                "id": "01e20a61-203e-4a49-906e-73581aafabc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad6623a-75b0-44e9-8981-364a7b723c67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21817953-65e4-4642-af73-4bffb89a72da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad6623a-75b0-44e9-8981-364a7b723c67",
                    "LayerId": "a62d6225-204a-40b1-9f52-4cb002b1f01e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 668,
    "layers": [
        {
            "id": "a62d6225-204a-40b1-9f52-4cb002b1f01e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "297aadff-6bc6-46b5-8bb3-4c33dce57921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1412,
    "xorig": 0,
    "yorig": 0
}