{
    "id": "4df92a59-4ba3-4d51-a78d-ab574b344b75",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path0",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "fc762cb0-ad4f-4ac0-8226-a5ed2812f157",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 449,
            "y": 132,
            "speed": 100
        },
        {
            "id": "7336c6b6-91c5-4df2-89a3-bdf26f26a48e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 265,
            "y": 137,
            "speed": 100
        },
        {
            "id": "855ed3e8-48b0-49c5-83ec-2a4658257f88",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 195.5,
            "speed": 100
        },
        {
            "id": "5db52218-c656-4291-9967-9e6208c95730",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 195,
            "y": 272.5,
            "speed": 100
        },
        {
            "id": "eb404d68-5fb4-496a-8171-fed0aabbd970",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 302,
            "y": 309.5,
            "speed": 100
        },
        {
            "id": "427409bf-db8c-429e-ad7c-18f2a0f0b8ac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 487,
            "y": 295.5,
            "speed": 100
        },
        {
            "id": "ef5fcadf-d04f-48b1-bb54-5f99df4cb374",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 541,
            "y": 200.5,
            "speed": 100
        },
        {
            "id": "cf8427e1-04de-4d94-a2e8-2489ea41a4b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 185.5,
            "speed": 100
        },
        {
            "id": "fc27976f-5445-4938-9e3d-50911621fcb4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 272,
            "y": 250.5,
            "speed": 100
        },
        {
            "id": "a42ed5ff-e469-49f6-9748-47a8ddaa113d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 370,
            "y": 276.5,
            "speed": 100
        },
        {
            "id": "187f6bee-c352-4ca4-ad96-84b33c8308f8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 428,
            "y": 243.5,
            "speed": 100
        },
        {
            "id": "98bbe7c0-1273-4565-bc25-2970e45f5bf0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 406,
            "y": 156.5,
            "speed": 100
        },
        {
            "id": "b22b208e-8573-4fea-9200-67aedaefec45",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 154,
            "y": 170.5,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}