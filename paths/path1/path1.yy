{
    "id": "0e7a8ab7-6db2-4108-9a06-dc18062ec31d",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path1",
    "closed": false,
    "hsnap": 0,
    "kind": 1,
    "points": [
        {
            "id": "3c612598-7bea-40c5-acc6-bbc1f81e1c0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1509,
            "y": 191,
            "speed": 100
        },
        {
            "id": "9dd79b00-6f3e-4e26-8898-f772ff3ae2ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 419,
            "y": 216,
            "speed": 100
        },
        {
            "id": "e1e09f96-321b-4b9e-97c8-6cad9a0c37d1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 412.351929,
            "y": 494,
            "speed": 100
        },
        {
            "id": "26e6b903-8774-4924-9ac2-5ad6a4e77092",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 762.3519,
            "y": 703.2593,
            "speed": 100
        },
        {
            "id": "9dd57870-0a38-43e2-8664-ba2c6277dc35",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1108.79639,
            "y": 658.8148,
            "speed": 100
        },
        {
            "id": "8a282070-d57d-439d-8161-6b307a5d6437",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1649.38892,
            "y": 560.6666,
            "speed": 100
        },
        {
            "id": "16af8319-be42-496b-a19d-be88bfaf6484",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1812.352,
            "y": 366.222168,
            "speed": 100
        },
        {
            "id": "6944a703-f15a-468c-85e7-2f98fa484373",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2001.241,
            "y": 262.5185,
            "speed": 100
        },
        {
            "id": "5b62101c-43aa-45db-9104-fb636266fc27",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2151.389,
            "y": 236.592529,
            "speed": 100
        },
        {
            "id": "c2f273f1-3068-48bc-a77f-694e52fde59d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2310.50024,
            "y": 243.999939,
            "speed": 100
        },
        {
            "id": "fdba3a5e-1af9-45c4-bfc0-b21452ef2f8a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2454.94458,
            "y": 238.4444,
            "speed": 100
        },
        {
            "id": "a829ffae-28bd-450e-a239-c05caf710eda",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2979.04858,
            "y": 203.327454,
            "speed": 100
        },
        {
            "id": "432e9c31-22dd-4de5-93df-6f66b0340a4e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3379.048,
            "y": 180.972656,
            "speed": 100
        },
        {
            "id": "2ed84f5a-6936-41f0-afe8-103ba75c621e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3714.3064,
            "y": 271.5855,
            "speed": 100
        },
        {
            "id": "ae10975e-e78f-4f0b-bf72-23f85ed548fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3785.758,
            "y": 622.779053,
            "speed": 100
        },
        {
            "id": "e8fb64e4-9704-440a-9274-a4373fda70ef",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3698.113,
            "y": 858.0371,
            "speed": 100
        },
        {
            "id": "788dbfb4-e2b6-48f6-9212-ed4865c5c0ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3410.887,
            "y": 932.1339,
            "speed": 100
        },
        {
            "id": "876799b9-7771-490d-b9ad-6cb4c1f1038b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2843.01587,
            "y": 854.9404,
            "speed": 100
        },
        {
            "id": "1b7498a9-c993-4cc7-83e7-6322ff20a200",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2672.597,
            "y": 593.65,
            "speed": 100
        },
        {
            "id": "825433d7-fbd6-4c96-b3fb-979d36360e65",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2695.17749,
            "y": 325.908081,
            "speed": 100
        },
        {
            "id": "81c9600d-e7a7-401d-b766-dcd48f9a6cab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2817.758,
            "y": 25.9081421,
            "speed": 100
        },
        {
            "id": "bafaf653-a6e9-4feb-9139-0dc231736d32",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2975.82251,
            "y": -25.7047729,
            "speed": 100
        },
        {
            "id": "1a89325c-3686-4a4d-9373-e80b5ab8e6ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3366.14526,
            "y": -128.9306,
            "speed": 100
        },
        {
            "id": "d084a94e-7933-4918-a09f-3eab87d64f6d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3698.403,
            "y": -80.54352,
            "speed": 100
        },
        {
            "id": "c7233490-12c7-4811-af30-be921ce90b17",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 3795.357,
            "y": -230.809509,
            "speed": 100
        },
        {
            "id": "6c6ce203-8eef-4076-a12e-cb31310e8bcd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 2647.40479,
            "y": -682.7143,
            "speed": 100
        },
        {
            "id": "2be80c6d-10ec-430a-a690-7c318b72ec34",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1852.1665,
            "y": -492.2381,
            "speed": 100
        },
        {
            "id": "9ca6d51c-d8cb-4e68-b415-4cf8f5c7ab45",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 504.547363,
            "y": -201.761841,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}