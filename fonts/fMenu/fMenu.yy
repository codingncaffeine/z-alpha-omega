{
    "id": "53962fbf-2855-4af8-83ee-a3e781f9298a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Conthrax Sb",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "da46e983-a6ed-4062-944c-ebe7b69d6829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e1c34667-0b83-4e5c-a4fe-19b307ced0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "59b29c41-ec78-428a-a770-36532d046fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 136,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "49b6a921-8675-4a5a-9d2c-7e144ae433d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 121,
                "y": 68
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "40498982-9045-4913-8286-cc66cdf9d381",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 106,
                "y": 68
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1dce577c-9851-44c8-9655-0ac4241178d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 86,
                "y": 68
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c4a7352a-1021-4467-848c-a3e2683a0905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 70,
                "y": 68
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "da4a8cb6-6776-4797-aef4-e525d0471139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 64,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "500c5a5d-5162-449d-bf05-37b997051439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 57,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5c292c7a-8eb5-4582-9e13-119c87faa52d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "38082235-8fa6-4d46-b6a3-ebe21e0a53a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 151,
                "y": 68
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "029793ad-dfd1-4721-abba-380c9a72350e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 37,
                "y": 68
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4ea199fc-1e04-45cc-abc1-7eaef5f954d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 16,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1462d212-f0c2-4f87-baef-1b5c0f270a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 8,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7956a6b3-cb6a-4792-b82f-7e58317c634a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f9e8bec8-a802-42ac-b3ca-734936cb168b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 244,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d761f63b-1dde-4e60-ba3c-7ced224ffa2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 228,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5c3a1e15-bec4-4d54-a6de-85a88c600f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 221,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4d35789a-e30e-438f-bc1f-96667eb59e37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 206,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "cbc787a3-1105-48cd-9284-4dc84bac9200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 191,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0524972b-37b8-432b-8268-63d27672e600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 174,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cf986ff3-4ac4-4289-ab82-ac27467c7151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 22,
                "y": 68
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "caa0309c-fae0-4e54-95bf-25104e7f71b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 163,
                "y": 68
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2df14b41-36f0-4b7a-bf3d-fbf7508adabf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 178,
                "y": 68
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bd7d7606-3e31-41cb-8a11-36f302c38649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 193,
                "y": 68
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12a847fa-4d34-4555-90c1-8d77c46ddac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 236,
                "y": 90
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ec87b0ee-2eb1-4821-adc4-d4397c437b2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 230,
                "y": 90
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eb9e06a4-b3f6-4d89-8cf8-1d80b9544648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 224,
                "y": 90
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d89d0ea4-b7c6-45f2-bfe6-fd544e939057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 212,
                "y": 90
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d682a5c6-fa40-43b7-a2d1-a83ea861a8d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 200,
                "y": 90
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9ae1dd84-a302-4d12-bb83-1005f8ecdf9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 188,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "37f2bc6d-f455-46e2-b79d-b939205de2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 176,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "20a0a3dc-9481-490b-a090-d9ccdaffe67c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 160,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5650ed9a-835f-4c52-bb76-13f103d29f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 142,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c6eb4681-b485-4253-844c-551c3016283c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "993d720f-aac0-48cd-bd5b-70c94448efbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 112,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fc26ed71-4cf0-4b2e-a544-2b1b451c2a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 97,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4f58303d-c9b4-4d51-b3a7-23de62f77d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4ea18b3a-22e9-4086-9414-4d1c435171b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 67,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d0158592-c80d-4649-9b8f-c6000f5ca53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 51,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a8dac2ec-f8a3-4707-8e15-392d7fe382fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 36,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0777ae5e-a016-4676-b6f2-fb5297e37b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 31,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "25d540df-d38c-495b-a4a5-3a22c5a1fdfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 17,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "531ad6e9-21ee-4996-9368-9ac770d38e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fc1aac62-07e4-412f-95bf-03aa3f371baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 229,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "048a4b0a-9d00-4718-8371-07ae86f39815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 209,
                "y": 68
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a4722b09-205e-4b6c-bcab-b0e17bd2d692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 158,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fe8e4dfc-6199-4ab2-8bac-695ef9a652af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 141,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7ab55bca-4ab6-462a-99b0-9aea692fe467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 126,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4fb9fe56-0ec6-49bc-b5e7-cb177699abaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 55,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b4ae5d1e-19cb-44dc-88f0-5b03ae2694b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 33,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "44f0e2aa-0881-48c8-8cc5-966c66f9a51b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 18,
                "y": 24
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "672aca6f-3c33-4930-bf49-db1c45b43443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6bf15e12-b6eb-48d4-8cac-6a08f4b677a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7ca4f03f-6e49-4da2-ba5c-c0cabe444831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ce830ca5-711e-463b-a198-c40f0a6417ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "76d56d11-32bc-40b2-9d41-d55a27ae03a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d789637b-6937-430c-9936-5724e1535adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5a10fb5c-af1f-4123-b815-83e2300fed93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cce0df7c-da0f-456c-a2fa-26bb7dbb8800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 48,
                "y": 24
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e7c8ca12-47c8-4abe-8ba6-82041c89f017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e0f294c4-7a7f-4860-92a2-21937e15a56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8f481343-bde2-4d02-91a3-ec1547adce06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "79e9c25f-8350-46d8-bd59-4c75e3d62ebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e2c2248c-bbfc-4dba-a32e-348f1b06522c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1092b26c-6556-4896-ace1-725e558b6418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1f6a0382-b327-4acb-b6bf-b6c6b72a04d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ccf8c4b6-aa86-4dce-afda-8782ac72baa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "577befd5-4b56-42bc-81f0-751217116c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f96063a1-e8f6-4eae-93ac-c1eb0beb9a97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1fd2b412-0891-40fa-9f9f-3f224dcb6320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6dc4edae-5be3-4301-9a19-8f3246974226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ca2ab6ac-d400-4a95-8c24-a0f5824c4442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 212,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3bac2dad-1e2c-4f7f-b247-0aa5089554f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 87,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "59172aa5-2337-4341-b220-f3af1dacff37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": -2,
                "shift": 5,
                "w": 6,
                "x": 104,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "27a5e545-ff65-4d56-ae5b-20aae74921fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 89,
                "y": 46
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1e9829bf-d8f6-4a27-9425-c68b6770a52b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 84,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "57c4bc2f-5e06-43fe-9eb5-a6de2978c480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8e0946b1-5c79-4875-8bcc-da0195dd4ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 49,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "40703109-57d5-4102-8aa7-0ad4f20ff6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 33,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "95205a61-615e-4392-b029-b88701e46eb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 17,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2f5dd2c5-bd4e-4b02-b8f0-7930357f10b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "d2a2aa42-33ba-4053-b50f-bd14dea61e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8b691144-00a7-44c9-91d3-bf1e95202f86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 46
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9e3efff6-32cd-48f2-9f00-fc4bc6cf8deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "023ea4cc-547c-47ec-a4ed-6cafb17d1b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 196,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "31056508-87c4-441c-b8a5-bec879026de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 180,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6a210081-7908-4bcb-afb7-2f9621887c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 159,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e494f736-9827-456d-b451-7299b98eea9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 144,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "624c625f-4746-4e5a-9d71-9ae24b3753b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 129,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cffcfcc7-4efd-4738-811d-486273eb287f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 115,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7a3f99c6-8ad9-41a4-97ab-b0d486fce91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ee87caeb-56aa-4e19-9aae-9efd8918341c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 101,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "10cbd5bd-b537-4dc0-ae38-90e051fc59da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "251b60f0-d3c0-4496-8003-4df6a1bdbe63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 112
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "81e337f5-df80-45e3-828d-f9f79e83b69e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 14,
                "y": 112
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a9667aff-aba6-427f-8408-2d717c30420f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "fc3ef07f-77f8-4935-aab0-219b54bfabd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "d2bd02a0-1813-4d2c-88d0-7823bab3122f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "67bfc548-d52c-472f-8335-041652b57fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "9eaf4d0e-e4a6-40a4-9f0d-290b605dc920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "f4d2e724-408c-4bb1-b8ca-c15f442d7a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "f6a47c02-048d-4dc2-99b8-d6d5421b5fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "e27c208c-c743-4a72-aa74-b2ba6f33ee70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 198
        },
        {
            "id": "31215e2e-cd14-49ae-b70e-b328e6b8597b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 256
        },
        {
            "id": "dc4cd99c-e5a9-4c4c-bb90-e510e9e890b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 258
        },
        {
            "id": "3bfe9a24-6174-4922-b053-4472ea6b980b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 260
        },
        {
            "id": "50984de6-e99d-4384-9ed8-052052415dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 913
        },
        {
            "id": "1611ebe6-0183-456b-873c-4c6fa4e48cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "b5074190-0e8b-4f7c-acca-58b77293f89a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 923
        },
        {
            "id": "bf4131e3-91cc-4e43-9f49-45213219e1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1033
        },
        {
            "id": "9ae41fee-73a5-46df-8e76-b820495e1d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 1040
        },
        {
            "id": "6c6d3a10-96bd-404b-9e1a-89715a4f446c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1044
        },
        {
            "id": "4a217b80-042e-4da3-b47f-603ae09043f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1051
        },
        {
            "id": "dddda57f-32de-446b-8b5b-d58185bc5413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 1298
        },
        {
            "id": "47ea5e5f-290f-4446-93c7-696f19f7b82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7840
        },
        {
            "id": "7593344b-c697-4550-938d-7c7e01210279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7842
        },
        {
            "id": "7031f7c1-3acc-4e18-beab-814419f34c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7844
        },
        {
            "id": "c2ff95ff-3932-446c-aa71-c91c523d2e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7846
        },
        {
            "id": "b5f43528-2ba2-47c9-8952-c3699bc3bd79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7848
        },
        {
            "id": "3afecc86-6870-4988-bacc-f85b6abb7f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7850
        },
        {
            "id": "72b2b0f3-e8a8-4d51-8362-afae64579e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7852
        },
        {
            "id": "26e6b3d2-9e13-4be8-9758-5106955abce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7854
        },
        {
            "id": "65d9e0cc-2593-4f8b-a924-c7c7edce62c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7856
        },
        {
            "id": "1ea4342f-956c-46d1-a9fc-720170e0dc64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7858
        },
        {
            "id": "cdf236f7-7bcb-4a5f-a020-64c3fa4f7280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7860
        },
        {
            "id": "cde54d54-58c4-4ba5-a337-ffbcea587fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 7862
        },
        {
            "id": "6689a7d3-a209-416a-ad5f-4dcd13dc7d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "e1e60f3d-5fa1-4c61-b829-b719411334d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "28c8ff92-38c8-4162-b32f-421e52814ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "0da50472-e07c-40e1-a345-75c4cf6f0374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "a23cd7e8-b89b-4427-a26e-a32c41c7853f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "2172aa4b-5117-4233-ab05-90b8e68f18f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "9a2012e1-1b86-4427-9f2c-fc32f5e18a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "e85a26c0-bdf3-462d-b63b-66aca3ab45b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 198
        },
        {
            "id": "9d6f4977-25cc-4b59-ab27-4e31e59d21a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 256
        },
        {
            "id": "e6e1eba1-3a0c-4f62-bafb-6546d76f8014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 258
        },
        {
            "id": "e74a5fa0-167c-48c6-a2e2-bdf366b76ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 260
        },
        {
            "id": "c74a9fb3-8a6a-4b47-b847-ef4e37c7956f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 913
        },
        {
            "id": "d000f9af-2364-4e57-be8a-92532fb15e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "05c8c977-0437-4fea-a746-33eed53fe813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 923
        },
        {
            "id": "7d5c39d8-c776-4722-a1ed-58ce5cd1ccf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1033
        },
        {
            "id": "29d3bb1c-babf-4aa2-9830-500002628e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 1040
        },
        {
            "id": "90f2a203-846f-41d6-8787-3734c750331a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1044
        },
        {
            "id": "d155de8f-266d-4590-a907-f32ad661edad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1051
        },
        {
            "id": "11e5766b-d5ea-4fd8-9296-12eb3db9f460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 1298
        },
        {
            "id": "2e1d0bcb-6531-4616-8ec7-d6005ad591fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7840
        },
        {
            "id": "0c4238c2-4dec-4052-9317-318d3ae854ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7842
        },
        {
            "id": "10d435fb-e895-4acb-829f-dc3f7a3e8983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7844
        },
        {
            "id": "1b3de592-004b-49d8-a03c-7990cb81d172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7846
        },
        {
            "id": "53d39e62-0235-4133-8ce4-f6b43ac8c3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7848
        },
        {
            "id": "d8f3e3ac-71b1-44b9-a116-96f8e02d9b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7850
        },
        {
            "id": "3b0a679b-2252-4717-954b-9c1c082dc18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7852
        },
        {
            "id": "44349a46-9a1a-4b7a-9921-c90a835ff049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7854
        },
        {
            "id": "15eb9c87-cbe7-4668-9ebb-f1ac28c2c16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7856
        },
        {
            "id": "07c0c615-70e9-4a5a-a589-0b22f71936ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7858
        },
        {
            "id": "6ab5a242-9099-46af-a5a5-10d79fcbebfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7860
        },
        {
            "id": "880e5a14-0dd1-4e4b-8131-e479c7d87090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 7862
        },
        {
            "id": "99432682-018d-42c4-9d31-535792fca32a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "d4fba65c-ab18-4be9-afca-58af8e6bcebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "35800af1-ec4c-4209-be1e-e4b3d93b3a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "664425ea-836b-482e-9bfe-edff34992fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 221
        },
        {
            "id": "0cf0c1d7-599b-4a34-a32f-f0bfb272ed2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 253
        },
        {
            "id": "547d7100-1b8a-4328-854f-941d92a8c3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 255
        },
        {
            "id": "3ca7a3df-9409-4ed2-a5f5-1fb76419e316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 374
        },
        {
            "id": "e2a8ec80-e910-4d8d-a221-2041c94b847d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 375
        },
        {
            "id": "d53ce2ce-d7ac-4693-8754-f021778d1644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 376
        },
        {
            "id": "58eca9f0-2bb3-4be8-b065-7474b7cab8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 435
        },
        {
            "id": "25d117e7-8c1e-4706-99f7-cf377c64c890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 436
        },
        {
            "id": "68355747-9b02-470c-a88d-a7f5b9adb798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 611
        },
        {
            "id": "d16edbe6-43e7-454f-b9ce-9b426c167c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 933
        },
        {
            "id": "cf5cce18-2620-4d66-813e-490054e4f244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 939
        },
        {
            "id": "be625fd1-360d-4310-a468-e1c015c61245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1026
        },
        {
            "id": "9c2a768c-5250-444f-aff4-e29b697b59f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1035
        },
        {
            "id": "ee722df1-54b6-4652-b622-b0e7ba407d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 1066
        },
        {
            "id": "8954914d-1081-42e4-9934-97dbf7533281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1091
        },
        {
            "id": "7ac25e66-8a8a-4512-9775-0b807cdcabd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1118
        },
        {
            "id": "97530156-b556-4e9c-9131-c29e517596ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1198
        },
        {
            "id": "d49f41eb-0e7c-4c4e-a1b8-1d7b41da6391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1199
        },
        {
            "id": "281162f2-82e1-4e97-b146-92c838099d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 1263
        },
        {
            "id": "524657e8-006b-4362-8222-10dcd9e01550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7926
        },
        {
            "id": "e33448a3-9bf4-41aa-8f82-e62f42915cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 7927
        },
        {
            "id": "fd326a9c-090a-47ce-afa8-753c74ac2097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1026
        },
        {
            "id": "efa8c9f8-f449-445f-a30e-c4a30c1b7630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1035
        },
        {
            "id": "485fbca1-c9e8-44cb-b170-b5f20a8077df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 1066
        },
        {
            "id": "9fff805f-754a-457b-9da5-4fd722c90044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "90821eb4-ada0-434c-a16e-a3d954208f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "b8570fa4-87a9-4d81-8bd3-64f98f6a4cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "271fcc6e-7c45-4336-be16-f52940664c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "3cce825f-01ce-4dd9-998b-92b8d62c7a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 253
        },
        {
            "id": "a0fb76b2-ac50-450e-b0d6-193a34074016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 255
        },
        {
            "id": "bd57ccc8-f8f8-43ae-bbfe-f59a6bb89678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "ddc9acb3-d471-4dab-b975-6192daa0dc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 375
        },
        {
            "id": "9fb0a053-bfaa-44ef-9821-fb8ebe2323e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "35406fdd-1d7f-49f5-a5b3-f1efd7b37baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 435
        },
        {
            "id": "db530ef0-bf24-4b17-ac15-65ee7865c7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 436
        },
        {
            "id": "203d5ba7-557f-4feb-a2d1-74ce5092fef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 611
        },
        {
            "id": "7ac3de7c-848f-40dd-9446-b5afd4bfb615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 933
        },
        {
            "id": "acda08d5-50a7-4732-ac42-2189352cb77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 939
        },
        {
            "id": "0e06e664-bd7c-4aa3-be23-dd47fc285592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1026
        },
        {
            "id": "cb3655c0-fa89-4ce7-9ee1-c846f59ca258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1035
        },
        {
            "id": "c78e5634-045c-4b96-96e6-39785e7c3b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 1066
        },
        {
            "id": "6968aeae-c79a-4152-9ee7-70b2d3328944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1091
        },
        {
            "id": "9f04e0dd-3d5d-447d-83d0-ea6c079048cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1118
        },
        {
            "id": "aeddcbaa-7ea4-4c43-93c1-607ed46d3bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1198
        },
        {
            "id": "bf99d5b0-4ca8-4886-b52e-0595532b368d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1199
        },
        {
            "id": "6946a68f-c11c-41f8-ae10-b1ec481f1bdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 1263
        },
        {
            "id": "79e6820c-4f96-4fe0-b56c-db9fec3e0c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "e8e153c8-4a06-4ee7-91ee-476e3c6d92d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7927
        },
        {
            "id": "47192f1c-1a1e-4179-aa97-49508f569f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 65
        },
        {
            "id": "8896992d-16a3-44fd-84fb-8d4b72a67493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "db720399-9570-4a10-8402-87767417df11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 192
        },
        {
            "id": "e4936fd0-3120-4c0e-a0f7-8b6d22111752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 193
        },
        {
            "id": "2e9e0ab9-462a-43f7-95fe-23b607b973e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 194
        },
        {
            "id": "a3b94fb2-8ccd-48db-8ba4-e69a11cdb788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 195
        },
        {
            "id": "86b4d00d-b321-4732-9c3c-86fddfaecd44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 196
        },
        {
            "id": "22dd7f36-a5ab-45bd-bd51-0627b723cd54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 197
        },
        {
            "id": "e09dcd99-685e-4bd6-9c89-bda3b9e75118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 198
        },
        {
            "id": "20956903-fc0f-418c-b392-7cc6702a7012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 256
        },
        {
            "id": "332fc6ac-f8f3-4a7e-9cae-155e6db9fd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 258
        },
        {
            "id": "60eb976a-9588-40a7-872d-45ac85b08381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 260
        },
        {
            "id": "9736caf4-e391-40fd-8e12-72826a663c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 913
        },
        {
            "id": "3594fda9-9aba-41ce-8665-18146c01356e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 916
        },
        {
            "id": "6832ca4c-d986-4d7d-ac7f-d39c7cffd888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 923
        },
        {
            "id": "35ba8fbe-7146-424d-bf16-e883e64d3ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1032
        },
        {
            "id": "46448d64-decb-498c-bd6e-8704951d52e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1033
        },
        {
            "id": "866cf444-2b9c-4599-aca8-ab41a5607673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 1040
        },
        {
            "id": "43f93c44-8fac-4fdf-94ef-4da810b03599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1044
        },
        {
            "id": "75f7d617-3e74-4d6c-afd4-5962542f31e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1051
        },
        {
            "id": "15ab7d7b-e2f9-4bd8-bf83-0d03e21552b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 1298
        },
        {
            "id": "579031f9-c7a2-410d-a2f4-3ad16fe42e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7840
        },
        {
            "id": "f19ae03b-7108-4fb7-b6de-5f0b048c940f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7842
        },
        {
            "id": "c49fee9c-255e-4703-9fc8-dfe29d14f1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7844
        },
        {
            "id": "219299ff-2776-4ced-b0b3-5aaa0f9500fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7846
        },
        {
            "id": "16556422-c21d-4d8a-acfd-bd16768f7c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7848
        },
        {
            "id": "67462d2f-d173-4f51-9899-a4e8c93531a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7850
        },
        {
            "id": "dbe43fa2-4ca6-466d-980a-399a4b9122b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7852
        },
        {
            "id": "d2197740-8770-4bc8-b802-284820ef87f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7854
        },
        {
            "id": "6302a603-4d61-4b02-be79-e8d1f73496a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7856
        },
        {
            "id": "69fe4ffa-0359-4bb1-976d-12738e14925f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7858
        },
        {
            "id": "639498c3-14c8-4265-a2c5-6dfba5ce2dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7860
        },
        {
            "id": "7fccd7db-d279-4ff8-b844-21cd32ee5f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 7862
        },
        {
            "id": "99a4a197-6f01-42bb-a3c6-dfcf518a46de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "5248e72b-91a0-43e0-8e60-6dd3aa715c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "af827118-637d-48fc-8367-8f80d09d35bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "259f97ab-7dd9-4828-a9c2-1b5c21547e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "d236de01-95b5-4eea-a48c-5fae54900a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "a595a267-c0b9-42b9-9bbb-537e7b16a23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "09173e29-b424-4186-9be2-06155d6cebc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "9e26508f-85dc-456e-955c-e2009e0d3d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "9767ccec-d53b-49f2-ab31-27759bd9a92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "800fdf75-2c3c-4fbf-a380-d2d9cdad7c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 374
        },
        {
            "id": "33b26b7e-6fb2-4e8c-a2f0-105b7af57d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "fc78c74b-0cbd-4eac-af44-157c39175031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 404
        },
        {
            "id": "9ac3e294-d927-44e3-81ba-9bae66561e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 435
        },
        {
            "id": "1b2fe20a-4b0e-4624-a26c-d978cdd00029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "17e1b917-95b4-489d-896e-236b7393e4ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 611
        },
        {
            "id": "ec169e97-67f3-4684-858f-d6bf4462f47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 932
        },
        {
            "id": "21ad9710-fc13-4f59-96d4-7ddf34f4afe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 933
        },
        {
            "id": "14e76700-64b5-4d31-b3b3-2b97711922ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 936
        },
        {
            "id": "44016325-d19a-4f46-b2df-2dd4a6cd2abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 939
        },
        {
            "id": "9ce18e14-3531-419a-a7f6-598bf17a041e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 964
        },
        {
            "id": "60bb0317-ea64-40be-8148-1184c36af18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 1026
        },
        {
            "id": "7af7d8e4-387e-4557-87ad-20c751d68fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 1035
        },
        {
            "id": "460717f8-cdd2-46f5-a10a-bb1561118f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1058
        },
        {
            "id": "17fcaaee-857d-4d4e-9093-9d962f9a3825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1063
        },
        {
            "id": "13f14556-a848-4a81-9c5b-cd33471f9b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 1066
        },
        {
            "id": "aa2968aa-2faf-4f1f-a82d-b73c2528de1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1090
        },
        {
            "id": "6ad5b65d-e644-47c4-8e9a-71749d005c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1098
        },
        {
            "id": "fb085705-2294-4354-b0ff-4e12c23f87b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 1198
        },
        {
            "id": "6f2f1bbd-f1f2-4fe7-b54a-db266cef7de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1199
        },
        {
            "id": "2766b986-d3b9-4e97-969a-5689ef95816c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 1206
        },
        {
            "id": "2698fb10-07b6-43ad-a762-b01452004bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7788
        },
        {
            "id": "5d69944c-0a49-4417-b561-198d0b398aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 7926
        },
        {
            "id": "80cdfa35-bd43-476c-af5c-e3905f53dab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "635aa9db-9de6-4fda-9e69-1f56cb25f661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "9edad6ae-6d26-4953-b2ba-779729baccee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "7eecef69-0bf1-40b5-ab13-b0bfbda7684e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "feafec36-d6cf-4e8f-87a7-53ebd6d0b320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "970f3c52-6924-4853-9707-17338bd6ae21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "51ea6ca8-271c-4b2d-a54c-08576d7e168d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "4f69f857-1b98-422b-8426-a073b593d350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "e30efffd-9c67-41bf-81f2-7a4efee92a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 354
        },
        {
            "id": "347af1fb-fce8-4878-ab62-3c3056c1915a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 356
        },
        {
            "id": "1ec874aa-c729-4012-8d53-0775a208ed70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 374
        },
        {
            "id": "89fcfdd9-17b6-4b5b-bc8f-96e4d2e302f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "91d896be-f1bf-4bd6-a629-8985d8226535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 404
        },
        {
            "id": "942a6d39-a953-49c7-aa9f-2c33e9488261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 435
        },
        {
            "id": "4d0a559f-8d90-48cb-8f8a-4b442cde6605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 538
        },
        {
            "id": "4f02e9e8-dfab-4d5d-a66a-c559270e2087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 932
        },
        {
            "id": "7d0e2963-4d47-4585-9673-77714e15820b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 933
        },
        {
            "id": "5c9b6641-4fda-4be6-8fee-b13ae5b83e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 939
        },
        {
            "id": "ccd6dbda-4b36-421a-8977-525ee6e6bfb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 1058
        },
        {
            "id": "ad06bd6b-1191-4b8a-b2c5-b8caa856093c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 1198
        },
        {
            "id": "4b389da0-27e1-4eb1-b152-464550721867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7788
        },
        {
            "id": "a8b20bef-f48d-44ba-a349-815d5d757f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 7926
        },
        {
            "id": "ae6c5e4e-fb53-42fd-8f34-a81a7e396c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 47
        },
        {
            "id": "bb2c8b1b-5a73-46b1-aafa-06b6be66ffe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7adf9773-8f3d-4001-84ea-f89b37a3d7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "16a3a264-6352-4e7e-8736-02ce13e9cb84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "08ca5516-b7ec-419a-ba44-807e8c749b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "9d504252-61af-4402-8461-4f20bc557cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "7881bc77-e8c1-47df-982f-f7d78747acea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "9f85a153-4e6f-4a24-9e85-5108af2cb11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "65b3d2a2-1ddc-4771-b584-c80f500bc3f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "9893323f-0106-47cb-8c79-93e373c646b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "354e874c-6873-4194-b7fd-fe2c056433b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "19897df7-32ae-4268-8d38-ae245b3cc989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "4c8330e4-1998-409f-b71d-3afdc557847b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "1314a43d-00c2-4931-8074-6380def01434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 913
        },
        {
            "id": "9e72625a-ee3f-4bdd-88c8-666efa05497d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 916
        },
        {
            "id": "ddf88d53-cf69-4da4-aec0-ac79afe1b270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 923
        },
        {
            "id": "b67bcc3f-beb2-4aad-8c1a-0a3ad3304ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 1032
        },
        {
            "id": "76bdc4c5-dfd6-4866-8a9d-a905d7be045e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 1033
        },
        {
            "id": "281eb6c2-c6d1-4914-ac35-b896745e0485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 1040
        },
        {
            "id": "f2ce57f0-f0bb-4756-acd6-3e89c6994d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 1044
        },
        {
            "id": "070206a6-c1ff-4e6a-8f3c-684acc49a891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 1051
        },
        {
            "id": "2a3f9958-50c1-446b-a372-5b1af3144196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 1298
        },
        {
            "id": "395c9f80-d49a-4c99-9e02-3e8cc82b8f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "44ebecee-0eb9-4c39-8fdd-e73be8e533d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "b5592a8d-dcf7-4d22-bc22-edecf884da1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "535dadcc-1d78-4ec1-a6ee-c9b507b95143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "7afa038a-d8a3-4457-9bb6-230dc437278c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "c908bdf5-b62a-43ee-9b59-c5e3fc26c67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "13ff6429-b94d-42ac-89d8-f53f81be1f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "49c5b5e0-9ddf-4a48-a05f-da6ed6970355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "f9664f04-1ce9-4b1e-ab0b-da72756b8663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "8964c8f9-a0f0-4e9a-9403-95a1197a9e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "78540579-73cc-4964-9a8a-70125cef40c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "e7ea8903-4a93-4dea-a574-c261206877d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "c29dc095-067d-4693-b4bb-31de8c50e720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "43db30dd-ebab-43fe-be3a-e8edda3dbc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "aa726cc1-7cdc-4457-b7ad-0969406178b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "e2350ce6-d940-43c8-8055-34dbbece1783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "0e7a1208-aa85-458e-acbe-776a5aef9ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "a3b4a92c-59ed-48ac-9e89-836822e1ba93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "122d6ebb-9636-4105-9ee7-9cb8f86c3d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "cbc1a006-9074-4a09-bb61-39d87e2310aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "35d4f2d6-0b0b-493e-9017-931774b56cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "5563a4ad-1476-4790-8866-bc18944a3fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 198
        },
        {
            "id": "2485019a-3cdf-4b21-8040-ffbacea40e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "d19fc9e1-665d-455a-8567-21c14ead73f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "4ec09a8b-42e8-4edb-b90a-90b8a375d184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "ba92b5a0-2a67-4cb4-964c-9b88b344210d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 913
        },
        {
            "id": "209917d9-bd93-472f-b206-057deb66fcbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 916
        },
        {
            "id": "3ec7d8c4-8679-445f-aee8-b624443ad2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 923
        },
        {
            "id": "cfddb077-07d9-4197-a12f-1e0ac500adfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 1032
        },
        {
            "id": "a1290d3e-ac1a-4e15-a234-5793ab2357a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 1040
        },
        {
            "id": "4c4120f7-672c-4291-8f12-2f862ae04748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7840
        },
        {
            "id": "45fe124d-d28e-4cd2-983c-60d5062e101f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7842
        },
        {
            "id": "49d3e677-f304-4ee3-8be5-20be3f60d5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7844
        },
        {
            "id": "3e03e88c-3e1e-4b6d-ba4f-304cf8f74992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7846
        },
        {
            "id": "b9781ac7-ff63-450e-8a18-38cf714a4744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7848
        },
        {
            "id": "b8426604-09f4-4fb5-8d3c-9ec3bc1d3725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7850
        },
        {
            "id": "a8260cf1-6e67-4297-9ac0-4a4eb277386a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7852
        },
        {
            "id": "f5279054-c2a9-4e79-9de6-f4bb590542a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7854
        },
        {
            "id": "4a9708d2-026a-4c6d-86b5-caf8c10dddc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7856
        },
        {
            "id": "60b629b0-dcd7-4456-b32f-31152c73f5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7858
        },
        {
            "id": "2009a47b-8161-49fd-a744-ef714f3a34e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7860
        },
        {
            "id": "36f8a4d5-5a63-49a2-a676-d1a76ce5ae7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 7862
        },
        {
            "id": "c6e8d32a-c92f-44e5-827b-d707b3516cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "1d39ba64-8f68-4e9e-8ac8-a510f935caa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "90ad5213-ed45-4016-a93d-7c2598d6c543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "01e0da9e-a49b-49e7-a9e1-950702dc3422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "fe533cdc-d94f-4ad9-acec-c9fa1d03e2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "a4bf1aa8-92c3-4ee8-9c77-b3b34fafcdf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "834827d3-dc38-4f63-af7f-4fa5d566dcc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "e59e8963-8221-4653-bc3d-a4dc1b625172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "3c30a3ae-b003-4dd3-8f0e-aab0e94fabcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "e70e87ab-453a-49af-8f74-a734a99436d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "c4d3b4e8-4183-4e25-90ec-391548e53c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "50c87d10-32de-40db-875b-59ab8cab4ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "7909f1cc-1d25-49eb-82c9-99b8133e95ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 256
        },
        {
            "id": "4fc7c3d8-16cf-4e12-9313-2a440eca6017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 258
        },
        {
            "id": "6c7bb08e-0f33-4f0c-8f2a-de252d92feae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 260
        },
        {
            "id": "1d15792b-48e8-404a-a785-186cd20bfcad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 913
        },
        {
            "id": "e8991fcb-02fc-444b-a916-4c0d560bdef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 916
        },
        {
            "id": "4714e30a-de87-400b-8ae8-14fb1f59dd3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 923
        },
        {
            "id": "f91b908e-e4ff-4978-afcf-c731f797ae83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1032
        },
        {
            "id": "51892c07-f747-43c8-8df8-4d1afc239c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1033
        },
        {
            "id": "7a49a03f-d5ac-45e5-bb48-049359bc0de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1040
        },
        {
            "id": "d29a25f0-375d-470d-89bd-4932ddccadf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1044
        },
        {
            "id": "cfe78110-8adc-45c2-a268-715d188b7030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1051
        },
        {
            "id": "78d99d07-288f-4b4d-aee1-0bd65e073d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 1298
        },
        {
            "id": "5e8f7e46-3044-4429-996a-1fe10a7b416a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7840
        },
        {
            "id": "4a84911a-c4f3-4b51-88c7-ff8cfcc6734a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7842
        },
        {
            "id": "a026adb0-029b-49b4-ad66-ab88dedfb6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7844
        },
        {
            "id": "ce18f5a0-2f02-493e-8073-202f4f3c52d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7846
        },
        {
            "id": "7479b259-aa98-42a6-9060-dc0103bfd43c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7848
        },
        {
            "id": "65a1b9fa-03ec-40b9-8450-344b41ddb207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7850
        },
        {
            "id": "d09c0d49-0bb7-4be7-994b-be6cdd78c657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7852
        },
        {
            "id": "56a974a4-a548-4c26-b511-2eb543741209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7854
        },
        {
            "id": "bc64254c-351e-4be2-a8a2-b9fabcde2b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7856
        },
        {
            "id": "55d419c1-ccee-4d61-824b-cbeb1547c7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7858
        },
        {
            "id": "c0b9cdec-712d-4fc1-8a25-96df0eb2cab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7860
        },
        {
            "id": "6eea640f-208c-4365-8f89-471b886c7b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7862
        },
        {
            "id": "5394a333-bb80-4cc9-aa15-03c585d66258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "1d85a270-e43a-4006-8c62-242effaf2e88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "c0635e8b-0dbb-44de-ba84-d50ac69037ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8230
        },
        {
            "id": "b1cffa6f-53d7-4af1-bf5e-e07feb4c21e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "18893f6b-010f-4cad-ac09-4ebb31ecef00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 46
        },
        {
            "id": "a440218b-b04e-487e-bfe6-2b382b1eea88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8218
        },
        {
            "id": "5142e78c-8c59-41ca-8279-d8a813b762cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8222
        },
        {
            "id": "9450ec42-4690-4243-b5ed-e9df8e57359a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 8230
        },
        {
            "id": "63ae524c-9dd9-4aac-b166-83fd658597f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "b66a4fd2-9220-4e3e-a844-aa8101974606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "edcaa780-2f1f-4dd6-95ab-6663a3492cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8218
        },
        {
            "id": "966d6b60-cd25-40fb-b83d-b7c672aab504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8222
        },
        {
            "id": "7fb577fb-01b2-448f-a095-aa68d62f65cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "8fc1b365-58dc-4487-b71b-4c13bfdf34b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "8df336d7-9e2e-4247-a031-51f36aed38ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "2bee78eb-1bc1-42b7-9522-5dff8b6be476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8218
        },
        {
            "id": "2ff3013b-8868-4196-8959-07f0e38494be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8222
        },
        {
            "id": "d259d3f0-34be-4e26-a69f-4a3854ba4f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "SemiBold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}