{
    "id": "3f5ae2bd-8e90-4d1d-95eb-49517e888a70",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMain",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "PROMETHEUS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0d44f66d-e476-401a-b6a2-d9ccc248f5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "473b83ac-75e6-4a66-a62e-76127b80c504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 183,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5d988e48-dbfe-4951-acbb-095d6ec443af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 68
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "921f4d91-8d50-4d1b-8d98-6579f052678a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 163,
                "y": 68
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "86a06f08-3516-4968-b547-a68f0686d659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 154,
                "y": 68
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "84f6193e-830e-4fb7-8cfd-840b15491122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 139,
                "y": 68
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "63f6638f-3652-4432-a760-7ea524331acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 68
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ed475cd5-4945-411d-9d35-721111ec9f82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f63a411a-a0b2-4bf3-b3eb-ce0d996f9df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 113,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d0641a88-7cd6-4710-acf8-7c1e2f839797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 105,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "89fafdcc-a53f-4031-86e5-f8eb35cc3e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 189,
                "y": 68
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9406624e-5492-4dd1-bd6c-bf636bf0ea43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 93,
                "y": 68
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c4730a8b-25ab-46ad-a19a-c90959ae3935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ba7980c2-6c22-47f4-9afb-8edb50108717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 68
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "41ff0c04-9456-4792-b521-da930c86fa1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 56,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9de59515-d0bb-4c1e-9bd1-9d76006085a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cd6fde39-e7b5-4504-935c-92e75514ca23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 68
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "12d60b6d-e578-4a8b-8eb7-fff3222838ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 15,
                "y": 68
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e8a287a7-e08a-4f0c-ace3-3d461ee1bd73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "65bbd383-486d-494a-844f-94bfaa6a97e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "74bf95bc-f567-45c6-b737-866ec62bc23a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 46
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "40c7400c-f3af-419e-80c0-8713cd356329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 68
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "20f8965f-7ad6-44b5-bfe1-5169b5b93792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 206,
                "y": 68
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ab7cb2f3-948c-4e26-a8a6-9898a348412b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 218,
                "y": 68
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "af6f5b76-e9ac-48bb-9f01-0a362ea24b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 233,
                "y": 68
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "541fa930-dbcb-45bd-a7fa-6ed738456ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 112
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e04caba7-d90e-48f1-84dd-9987e7bcc151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 3,
                "x": 23,
                "y": 112
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c716ecc5-711d-4472-b875-ec4d3358cea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 17,
                "y": 112
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "2ef14409-3b23-4fd5-83a2-0155e225550d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 112
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "aa93d6ab-f0e9-425d-9e48-767a68b4906e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 90
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "eaec030d-0eab-474f-85c2-da177c30e049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 218,
                "y": 90
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ce32d705-282b-4131-80a7-ad3ea927f5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 205,
                "y": 90
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bbfca628-6175-4ef2-98bf-9d092457f6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 190,
                "y": 90
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7aa90c63-58c0-4bf9-b952-8d44c2a13de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 166,
                "y": 90
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e0974920-f8f8-4bdc-8d3b-a0ea966c5088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 153,
                "y": 90
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "28d6072c-25e6-4af3-b33b-52acd4f4e40d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 133,
                "y": 90
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d6dc696b-4e46-4524-a289-df64acce68b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 119,
                "y": 90
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b9416a3f-951e-4d49-bb49-37e3f7e5b429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 90
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "739bee52-a3cb-4158-8560-f33b8ba120ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 90
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "14988081-8acf-428d-b6b8-d7cdf8fd3b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 75,
                "y": 90
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7f9c7d0d-7991-4221-a054-e482e8717363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": -3,
                "shift": 10,
                "w": 13,
                "x": 60,
                "y": 90
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "48f33b4d-5211-436c-9f9e-dcbb639dec53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 55,
                "y": 90
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "68313fed-9690-49ce-a6c0-a1979fa0d402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 43,
                "y": 90
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d563820d-d3a6-42ad-b0a3-c904691bfb7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 32,
                "y": 90
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4ad3144f-9d75-4add-ab4c-800fc9089258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 20,
                "y": 90
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e1b0fa50-8edd-4e38-8bef-a186ad1a888c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 90
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f70b4658-41c8-4279-b7a9-bda4a7c52ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 201,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8ffe4bfb-9480-44cf-8855-1a4b39134549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 46
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e84e24c2-0325-4337-b91c-53ce07b9b3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 168,
                "y": 46
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8b8ea154-f2a3-49da-bcf7-404cca5a32e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "733146d9-9dab-4239-8b88-751e3f603134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6d320621-bd51-4579-9a5f-fa555fda604f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 59,
                "y": 24
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8d2fa907-13a6-42d3-ba6a-331fc2b043fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9cf7fe16-e86f-4627-9c89-d46533dd3dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f632b51f-3648-4916-9f9a-d9dacb0cefa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3c061f8a-e2e4-4f00-a5db-a27e7e5fa4bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 33,
                "w": 33,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "52c37813-1e64-45b2-9595-5f951ca3418d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "39cad47e-ffd2-4f16-a8ff-2b439838c2f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "57fe059d-c57b-4e5d-98a7-1fcadd980838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6be7d591-4f3e-4f81-87f9-9dfcbdad74e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 5,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1fd01e3d-5776-497b-89e9-1f7de2dcd82c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e94c9263-7e82-449c-afdd-cc8ae8e387d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "83bdf3c7-682a-4d09-a928-84b9df956ba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e2cbabdb-2d56-4087-948f-5b3b9480b764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2091073f-000c-4b29-aa93-88dd06a862ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2a54bcfe-39db-4013-842a-ff385a5d8277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "194270eb-5d41-4966-a4c9-a594d5d86572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7a4fc5c0-66cb-4db0-a1c6-34518ecc9b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3dc5da67-7a23-4fe8-83c9-76e9ffec25bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "49b95e5e-5846-4087-9bba-15a33b8d6324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "55308ba5-6bde-4aa0-8f9c-28ac70076a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6e6e8d49-6899-45de-a944-237134f0e4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 113,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a59ba1d5-89e2-4f00-b18e-6528a9d13a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 16,
                "y": 46
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "93ddcec5-3fbc-4ccf-b199-c9d050261319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 129,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "52c2bc8a-416c-4886-b258-392aae010fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 148,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "56e02fdd-e82c-4fdb-b5ee-c74cbc0519ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 139,
                "y": 46
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d6dccc5e-6487-4900-aae0-22a162d73002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c8772c3b-5d00-4cb5-ad9b-b87e6704a447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 114,
                "y": 46
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "160d5bce-0f18-4352-8f25-e8cbf02c94d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 99,
                "y": 46
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ee02f003-acd6-40fc-a90f-75a05d1211c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fbbd6e3a-caae-4004-83f7-1059a6c59a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 46
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9afd593b-acd7-4270-b537-3f294a69315e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 55,
                "y": 46
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ee2ab620-f0a4-420c-bc61-7f35b527240d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 42,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "db706d07-5b8d-4b69-9f4a-c1742c8bdb9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 158,
                "y": 46
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4c235d9d-a22f-4895-b41e-cc0ef00d7430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "06b9cc89-d84a-426b-8e6a-20cc0fdcf155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8ccaff74-04ae-4d28-8099-2e2a5a014fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 230,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "55e6cc10-d8e7-4c19-a6b8-f355e99e45cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 202,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "53ccf060-8d87-422f-9f8d-7be4ec1babc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 187,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a3a697ec-2223-490d-afb9-c0a92503584d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 172,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "11a6d52e-b897-43af-b2f5-be41b78c40d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 157,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "62f2c68e-8eb1-404f-b57a-a361a5c86654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 148,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "05c99c3d-a502-460d-987c-addef2a86c5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 143,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "466b4d99-a21a-44ed-bbd2-9844959296ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 134,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e8598d3a-3236-4973-b581-3c67d05176a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 112
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4a7f9727-03e6-4d94-a4c0-c8005c3a439f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 20,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 50,
                "y": 112
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}