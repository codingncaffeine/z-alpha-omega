{
    "id": "16940f7c-920b-4b46-b931-5df322dac48e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Neuropol X Rg",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a39affd8-4186-4f86-85d5-45e9332ab3e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "006a902f-e6a0-44eb-a00e-70648af14551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 127,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "588b4f33-a843-4898-8330-1a083e3351ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 119,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d289dbda-56a2-4738-9d02-42f76a48c5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 103,
                "y": 65
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6d645e49-9da3-49c8-a164-e93587c8fa6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 88,
                "y": 65
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9d383da5-70ed-406d-ae88-71c3fa953e53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 67,
                "y": 65
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "18fa8e09-e332-4384-b4b9-8b0eebecb315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 51,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9615284d-2213-4f2b-b443-7b31b507bba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 47,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0d135825-9ba1-42dd-bfd5-e9b17d6dae60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 39,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9fb203e4-a0c6-445a-9fb0-584e61fb7e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 31,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e70d8b6c-6160-406c-bc7c-a05d5613fc6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b423681b-129f-43f8-93e8-7f8623155406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 19,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c59e0314-1e74-427c-8c9b-f922b89b4fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 245,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e7e0b2a2-17a3-4d12-99ce-e5cf26c70a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 235,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a3d2b091-70b7-4fc7-8abb-55b794003f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c9090d42-470b-42b1-a815-3d2f4a8b1363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3c6433f4-24c4-419e-a7ca-21367a279b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bb8c2b9c-c295-4c53-8075-556a597f77b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 199,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5cd5efc4-0e22-4aea-a00a-0d88f0d0ed6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 183,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "80dc41f5-3913-4911-ba97-0567ad52a636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "83b0c146-4ad6-4a6a-9b82-2429df45ceb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 151,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6c5b3e2e-8f8d-418b-8bcc-f551c9bce2ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6e2cda6e-b6f1-41f5-a638-39c49bed1b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 143,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "87d3288c-7273-4abd-86b8-03112cda75d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 160,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f17c7666-33cd-44e3-ba46-a82ae4c78c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 177,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e7bf6bf9-15ce-41e9-bfb7-98aeed0da0aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 207,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "922eb468-634b-459d-88bf-4d5a8a4d1c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 202,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c8b4e881-0e77-4ab6-a641-101acbedf290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 197,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1b80642c-402f-497d-9488-ad06d60ad0ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 185,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0bf255ed-587a-49e0-a92e-3ba130844447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 173,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "328e2df9-8f1d-4723-b209-7c1d3aa52d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 161,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6a8402fc-6bb0-4abc-a790-5ded14ba4026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 149,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4fe34aa3-c6b3-4dc0-93ed-ce981d45bdf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 134,
                "y": 86
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6e232e05-59e9-4719-bfac-d618c98b5d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 116,
                "y": 86
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "308eacc6-cfb9-4804-abcb-ca75f48df2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "737bfc79-1fc2-46d4-b84a-6065e53493bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 85,
                "y": 86
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "acac91fd-363b-4b70-b47e-b7cd27c252e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 69,
                "y": 86
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a94e2aba-53a5-4b53-9a93-01fbc6d85468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9227abfb-ce30-478e-845a-b4493c685de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 39,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9c608be9-6018-4441-bfc0-ba6ed18a771b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 23,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "43cade8c-6eb2-488c-9115-78f1c1662aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 7,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0cd72e23-f3ca-41ee-95fd-4c687fd28823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a6357159-9244-4dc5-b3a9-4dbfce2ab2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 244,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "fda60af2-6afe-476d-aedf-c980c3c25574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 229,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "11a55860-51c3-4ae8-a0ad-f4807a37e995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 214,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2fb53d83-bfed-4419-9655-5371f8ef296c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 194,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "455a1704-abea-49f7-bd3b-a6cc7a3e5373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 135,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0d4ce576-a83e-4b97-bff7-57fe82325039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0e06507b-8608-45a4-a261-a3cc491f6ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 103,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8c74e6f4-319e-4762-8f73-638dac0a8387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7880637d-8877-4135-87cc-e8195eb90f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 32,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0c6c2031-3d8b-43c4-bd23-06879dd4e721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "44eca3c4-0999-4ca4-8b64-7c0372c154e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6e580803-b1c9-4623-8133-8577f5ee6201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bf77215f-4e69-4523-8354-73c3588f1e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9ef0fab1-4ab4-40bf-b36e-1563cfee6d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "beb2e6ab-1199-42fc-9913-7ea603231bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "642a6ed4-8762-4e49-a61b-5584a60769d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "32ec8ebf-c880-4b6c-ba21-ca8f285b23f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "961a78df-e69e-4d61-aeb3-15cfdadae225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 48,
                "y": 23
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "06e8f345-32bf-4fc2-a6f0-22df587a4c07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2932b984-f16b-4b21-aea1-532467a1aaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b8d0a2d8-c3fc-44d6-9dcb-49f210d5c0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "66e6b95c-8869-4445-a563-105c65601167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 14,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bc150d47-f970-4894-8727-6759b4607343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "571c27e1-9712-4504-ac40-c2019a307ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3a528849-d930-466f-8c56-09cc6bb6d6e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "56f1b0f1-8613-4414-aeba-b2f53f00ec3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "564e4847-f138-4f78-9a3f-8cafbc2c0109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c42ee497-11d2-410d-b903-4c52e2d160bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9226709a-7e79-4b49-b7c5-72099981165c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1c7fa05c-31b8-4312-926a-98d81680f6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "43e6bf5e-53a6-488a-b3a5-802387318651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 204,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5f05e19a-4f04-4a39-9cb2-0079b66b1f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 86,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "80b8f882-a3b5-49ae-b767-a74f3cc69d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0c162788-45a8-4256-a4fc-e80a72f69cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 67,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "727d76df-5e05-4a8c-8a7b-9b05ae2e9879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "50b67c38-f2ce-4be4-9f83-1ae5be5181af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "834a004b-9a4f-45d7-a8f3-3aa64e29b7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "603a7802-b056-4fb6-9026-17a8193f5bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3d58de7b-6d9f-4f25-b0de-6626cdfd9366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "03f54ce8-88c4-454a-b80e-37c3335404ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 238,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "97263fdb-7eda-451c-8a12-b36b2f85941e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 229,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fa7c322d-1836-4bee-856d-2f521f0a9c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 90,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "27b929a5-16c7-47b2-9f21-d1c5734919c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "366a1199-b642-4830-b3f0-1bf5841bab2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 190,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d067f56a-041a-4c5e-b30c-be368e4b57d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 174,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "694f2f72-dd6d-4eb7-99db-c87b44266b59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e312fcf1-a6fe-467c-876c-ac50395467f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 141,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5e4b75bd-fb4b-441f-9433-b33e1baa178d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "03d62e32-1ceb-40ff-b3e9-c4b7d6830d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 113,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "404d9cc4-4302-4069-a26b-aa04422705d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "31feae2b-f344-43a7-8667-55c0fb113be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 100,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "32d545bb-7c70-4c08-9988-d37d93ed8be4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d780a49c-1894-441b-b1b8-cfd12748226b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "09ad605a-5b9f-429a-b8a2-982f0850865b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 235,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "54b58ad7-e360-47c6-beab-cab8941c906c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 34
        },
        {
            "id": "6107eb35-ee62-4802-903b-25cb69847e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 39
        },
        {
            "id": "99da31bf-e950-4575-9fed-f313d1d74d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 44
        },
        {
            "id": "465b0bd6-4bb3-4414-9e41-c582353f7911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 46
        },
        {
            "id": "d6d90677-4505-4463-9b31-699118848db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 47
        },
        {
            "id": "8a3e5d48-d731-420d-95fc-3d145b83cf74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "031a7233-a4c1-43ee-a90c-11bb2dbb8051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 65
        },
        {
            "id": "766b81d3-ddd2-4eaf-9fac-f1f8ac24a88c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 74
        },
        {
            "id": "ae140230-3b99-4f71-a255-b1d004c6e8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 97
        },
        {
            "id": "142bf3e8-2d94-4f2c-8a40-61a21627a333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 106
        },
        {
            "id": "51d51f53-fdec-4137-9f6c-90b47084dc69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 193
        },
        {
            "id": "5dcb08bd-9aa4-4888-b840-30621cc25421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 194
        },
        {
            "id": "1d689aff-ab38-42cf-a15d-bf4f1838808e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 195
        },
        {
            "id": "42459371-ffff-49c5-a7e9-1ba350e3855b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 196
        },
        {
            "id": "0c883f08-d357-4592-bb14-8dad4b7a2ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 197
        },
        {
            "id": "53fd107e-16ee-47ac-b018-c80c7c0bbc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 198
        },
        {
            "id": "babdb31c-4a29-48ec-ab56-8b5b31a471b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 224
        },
        {
            "id": "3b4265cb-d3dc-4047-a630-cdb16c881759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 225
        },
        {
            "id": "9feebc30-d5cd-420b-bac2-a45352f96fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 226
        },
        {
            "id": "0131554e-143c-4e1b-96b7-eed23bd104e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 227
        },
        {
            "id": "5c10bfce-7de9-477c-90aa-0907a9c36fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 228
        },
        {
            "id": "b23d1dae-3fa3-4632-be1f-7df28978297b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 229
        },
        {
            "id": "a70dcff7-9689-4aba-937a-9f72e2b9e105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 230
        },
        {
            "id": "f2a55cc9-77bd-47b3-bb36-6f50eb397001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 8216
        },
        {
            "id": "89bd5a05-5f00-4725-9184-37d51e87b3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 8217
        },
        {
            "id": "8b3b3189-1f30-47c0-9c86-4184b52e8800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 8218
        },
        {
            "id": "a6010fba-0343-4712-b27c-334c30de2642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 8220
        },
        {
            "id": "60bb1ddf-09c3-49f3-a430-b72841cdb795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 34,
            "second": 8221
        },
        {
            "id": "6574af7b-3ecf-4c87-ace7-61b6b324a152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 8222
        },
        {
            "id": "104c97f1-8c85-4c8d-a16b-f951820b8ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 8230
        },
        {
            "id": "26732e3d-d051-429c-8f3b-0ea01c2cc4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 116
        },
        {
            "id": "ebf08820-d4f1-4aeb-bb45-c923d5f1366a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 118
        },
        {
            "id": "9990e09b-971d-470b-8019-1121d7a63b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 960
        },
        {
            "id": "19c12998-9345-48eb-8b7f-0b8fdbc75551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 34
        },
        {
            "id": "755fbe8f-1494-411f-acd4-f0e0664f3268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 39
        },
        {
            "id": "c36f626f-b87c-4b94-83fe-a6d52d0ad6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 44
        },
        {
            "id": "562c8606-f069-4ac6-bdfb-6b6f5d8561c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 46
        },
        {
            "id": "45c91746-a14c-4446-bc22-31af5e29d5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 47
        },
        {
            "id": "0b3d7dbb-1399-4d6e-aafc-320fb0253c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "c78810fc-42bb-4fdb-8714-1c038e9f8630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 65
        },
        {
            "id": "2203c6b7-410b-47ab-a6cd-1c1d0874854c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 74
        },
        {
            "id": "9e0243e9-9edd-4563-8881-e46d01b75bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 97
        },
        {
            "id": "1d790374-9d38-434e-ae01-be1d776a0b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 106
        },
        {
            "id": "8cb208db-0be3-445b-ae59-ca63576f0c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 193
        },
        {
            "id": "913b4d88-22b0-4e5e-9a88-0aea81195ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 194
        },
        {
            "id": "9541784e-8e57-4e4a-b5a7-a4a47f8e3bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 195
        },
        {
            "id": "6d097186-a8d5-46b0-a8db-df73ad166879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 196
        },
        {
            "id": "fa4547cf-4de7-4390-a908-5c0d12bbda6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 197
        },
        {
            "id": "e8a9bc4d-c7b3-4953-ad56-de6bfd7d8be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 198
        },
        {
            "id": "9ecb144b-3c10-4ee3-a905-42bd7003af75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 224
        },
        {
            "id": "15b9ce7c-b893-4eee-ac22-17d8d196f250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 225
        },
        {
            "id": "6ffc375e-7fe1-4be5-baf6-60a069b5dd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 226
        },
        {
            "id": "db525f1f-01c3-4bd7-9b1b-539e0d9a4952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 227
        },
        {
            "id": "028a51cc-7220-4666-83ce-0f14692873e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 228
        },
        {
            "id": "c62ad15e-e9f8-4adf-a671-6e9edfa3a54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 229
        },
        {
            "id": "44510139-571c-480b-93a9-f29ca1b93a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 230
        },
        {
            "id": "350f376f-a47c-4fce-ab65-c658ad5656cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 8216
        },
        {
            "id": "a86d945a-b8f3-44b3-b9c7-149021c9baf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 8217
        },
        {
            "id": "cfcf7375-f4b5-4fb7-a1d5-9e311b0cced5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8218
        },
        {
            "id": "487f46e7-5121-43a0-ac0f-8d64c7c5c19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 8220
        },
        {
            "id": "0ba167a1-b9fd-4881-be19-671c470c116d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 39,
            "second": 8221
        },
        {
            "id": "3a0ac0e1-37de-4d85-9b5f-3e534136199b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8222
        },
        {
            "id": "938b8af3-13b6-4179-8d3c-24f6e7c5599b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8230
        },
        {
            "id": "5ed5ece6-8d58-4644-bd89-a40ce01dbd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 40,
            "second": 106
        },
        {
            "id": "60aec1bb-68df-490f-8992-482a43909fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 34
        },
        {
            "id": "a25b36fd-5cb2-4f2c-994a-3bf2d026c675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 39
        },
        {
            "id": "07117ce1-d494-4054-916a-f66abde424f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 45
        },
        {
            "id": "aefbcd6c-f730-46cc-92de-943dde4e7c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 52
        },
        {
            "id": "efc174da-4897-4933-bfbe-43086959ded6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 55
        },
        {
            "id": "3967500e-6953-424b-9db9-62d01091ba25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "4af5d772-6cb3-482e-a5c6-835363b4129c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 84
        },
        {
            "id": "44986118-c3b6-497a-94dc-ac8c9444b05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "8d8273bf-1b4e-4738-9cfa-3ac678e2900f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 87
        },
        {
            "id": "a0e80361-1ee1-446a-a0dd-7e0c975af55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 89
        },
        {
            "id": "b1f22ca7-d9a3-4744-9de0-fa06606b25ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 116
        },
        {
            "id": "9d95e760-3973-45fa-acd1-673d5a99b64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 118
        },
        {
            "id": "b37ce156-e59d-4996-899e-dd8d568a98ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 376
        },
        {
            "id": "46140217-891a-4727-b10f-95ea77164636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 960
        },
        {
            "id": "0514f3d2-c443-468f-922f-2a1c89a09602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8211
        },
        {
            "id": "cdecf030-0118-40bd-be17-b85ddf9aec07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8212
        },
        {
            "id": "0d5c9fa5-2b01-442b-87d3-88cc45f7471f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8216
        },
        {
            "id": "8e8dfc84-2579-48f5-aabd-28b8576abb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "48548ae1-f67a-4772-9983-098b39c75e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8220
        },
        {
            "id": "12a5a15d-6ffe-42a7-9fb2-a2cadfad5892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "c0a9bc8b-5ab8-4163-b3a8-07f67cc72a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 44
        },
        {
            "id": "ceb89df3-23f8-451a-8e81-1bae521597fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 46
        },
        {
            "id": "56f2ef89-fd6d-4326-a4a9-0eac3a4ba231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 47
        },
        {
            "id": "94c5c169-eef2-4888-b98c-dc0a1fc2ed97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 55
        },
        {
            "id": "e4e37582-d0ee-4e94-ae52-a7e1fa84c883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 65
        },
        {
            "id": "49cf4343-f0e5-4986-a81c-135d5118da7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "1882d3b1-5735-4e3f-9dfd-cd73954e0183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "5860d92b-e500-4a43-93bd-33552774628e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 86
        },
        {
            "id": "27ce3070-b25b-44bb-bf2b-5219685a196e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 87
        },
        {
            "id": "1445c7b4-313b-4554-83b5-7f423c2b5a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "4279c035-6bc8-4ab0-8eb5-86ff1d02fe16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 89
        },
        {
            "id": "3c61fd00-b101-4365-bf08-3e19caf08710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 90
        },
        {
            "id": "0cc22a50-3c69-4ccb-b558-4cc131dc5b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 106
        },
        {
            "id": "e71d3916-7732-4dd5-b643-1af5539b45de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 118
        },
        {
            "id": "6cfa4e26-df85-44da-82d0-883e91425011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "72b5cffc-178f-44c3-8d9d-ce5e0850c1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "4a90d47a-08ea-409a-94ad-4be95ed5b921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 193
        },
        {
            "id": "3953c859-03cc-4d65-9354-48934c3d6e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 194
        },
        {
            "id": "e3f654d4-74e1-4483-abcf-74ca8b5ed57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 195
        },
        {
            "id": "6bd03dc1-f0cf-4747-ab18-88e562c6958e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 196
        },
        {
            "id": "0f728564-bf17-40da-9c25-594f67c4e062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 197
        },
        {
            "id": "35fb679f-5164-4a2a-9fb8-9b6a073aea6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 198
        },
        {
            "id": "04c350ad-ba55-421d-9281-2e073c0a071a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 376
        },
        {
            "id": "b08a140e-6530-427b-98cd-c1307942616d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 8218
        },
        {
            "id": "f021bcaa-c86a-49b6-8dbf-e6382c04642f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 8222
        },
        {
            "id": "fb99f1ce-f157-45b3-9ea0-c4b11011e75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 8230
        },
        {
            "id": "d6ae1222-4c66-4f8c-8d2e-b6c33342ede0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 34
        },
        {
            "id": "31b6b86c-c412-4946-bf29-51ca55f0a93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 39
        },
        {
            "id": "3bac4436-725d-4dc7-8fed-253d0893bd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 45
        },
        {
            "id": "ec515544-7df7-4809-bc1a-a37daf1dd12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 52
        },
        {
            "id": "0129f1bf-e838-4ca6-b0fb-aa06e5372b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 55
        },
        {
            "id": "843c30bc-a265-4de1-aafd-a88e117d0c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "a8634888-9acd-447d-8928-113cc002f943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 84
        },
        {
            "id": "bd6f3f15-4ea5-4728-9566-705548b40908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "7704e42e-afb9-45aa-9f9e-ac180963507e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "99cc4bbb-939b-4fbd-b760-b8b0c58c10fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "7a3c3e6f-095d-4177-8222-b52a0ddd1043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 116
        },
        {
            "id": "d90a006f-3c93-4c95-91b0-90f3042f7ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 118
        },
        {
            "id": "cdff7355-4bc5-41d3-9268-1f1da3f58171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 376
        },
        {
            "id": "686304ec-252b-4726-9c14-f26ffa41c813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 960
        },
        {
            "id": "f5b2fa36-6f05-400f-b3cc-9eef5660b199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8211
        },
        {
            "id": "bb9769c5-b63b-43a6-b9a6-758bafed2d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8212
        },
        {
            "id": "1182bb5a-7507-44cc-9b54-7dddd48f26af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8216
        },
        {
            "id": "80589c7b-284e-496e-bc9a-f62ec1b6a686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "074d1f9f-6c4a-4832-9074-a97b5a575a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8220
        },
        {
            "id": "2bee3088-320a-4294-b70a-79af78b50709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "26de50c6-f31d-4255-a19f-70fcecc0c979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 38
        },
        {
            "id": "2b4d9318-a216-4654-b1f7-9cf417639db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 45
        },
        {
            "id": "a318e884-412b-4bb5-ba37-227d4417304a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 65
        },
        {
            "id": "4a2dc403-e24d-4470-8801-7eeaa0181090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 74
        },
        {
            "id": "89900c32-c80d-49bb-aeca-da95c6ac1d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 97
        },
        {
            "id": "ed24c103-5251-418a-a7a6-2ba7e0022d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 99
        },
        {
            "id": "7d197237-b6f5-41df-9b8b-f33403fe296f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "684335df-1bf7-41fb-9a51-0ec34d61bdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 101
        },
        {
            "id": "c9c78dcd-9af6-440e-b10e-05e104847e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "4a4ed18f-a319-4f37-b584-aaa2ad2e0b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 106
        },
        {
            "id": "fedf62ff-1b4b-4b82-8bbb-40663bb905d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 109
        },
        {
            "id": "9acb73df-65cc-4c29-9a32-16693ee40a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 110
        },
        {
            "id": "017ba711-0473-411e-b4b2-560823baa4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 111
        },
        {
            "id": "daf4a706-089d-4db7-8b90-2888ab3120ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 112
        },
        {
            "id": "797ed68a-e355-42bd-9356-6db27763de4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 113
        },
        {
            "id": "0449abbb-1bb7-4400-b3fb-eb04dc9c7400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 114
        },
        {
            "id": "076c31a4-71e0-4f4c-970b-ea8ad1426fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 115
        },
        {
            "id": "c513f01d-2d45-4b8a-b552-1ffd864a0d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 120
        },
        {
            "id": "d4467fa8-5620-4276-9712-fe19bf879535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 193
        },
        {
            "id": "377cf878-6f8d-427b-934e-8d171982ef89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 194
        },
        {
            "id": "10cc5c42-ce5f-49fc-ad44-ed27ade96d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 195
        },
        {
            "id": "c042ab69-8776-4639-ab48-8d5b6db76916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 196
        },
        {
            "id": "b6fc3dd3-5617-499a-869d-132bedf2a71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 197
        },
        {
            "id": "0e40da90-7b45-40a5-b1ec-3a805079d87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 198
        },
        {
            "id": "4a42377b-7bb4-4ebe-8546-d6af02c87e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 224
        },
        {
            "id": "5da7111d-fff2-4284-b831-b5572177d94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 225
        },
        {
            "id": "7e3cb300-fef1-4bbb-9102-b7843a5f21ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 226
        },
        {
            "id": "19b8dc69-a171-4ec2-9d6d-d71dea0e6cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 227
        },
        {
            "id": "b27e58e4-cd87-49b0-8b5a-e3d1eb4ab539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 228
        },
        {
            "id": "9194096c-25ee-47ca-bcd1-7963c0b120d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 229
        },
        {
            "id": "3d304ea9-0e65-4848-b256-2c69520146b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 230
        },
        {
            "id": "4237ccac-5e0f-4a43-8fb3-866f9dc95edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 231
        },
        {
            "id": "aaac0879-54c9-4ebb-8d6a-c62b89449590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 232
        },
        {
            "id": "baba799f-42dc-40fa-8677-0f01c7c279aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 233
        },
        {
            "id": "1dc06b9f-8c98-4de2-af94-7948adefcb9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 234
        },
        {
            "id": "c8d0a09d-c1a2-4d46-8333-cbd40ced3cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 235
        },
        {
            "id": "2a3e10ac-6ec2-4c6b-95e6-d1a9d138d19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 241
        },
        {
            "id": "df56110a-696c-4c35-a47a-168f20acc876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 242
        },
        {
            "id": "d6d95f94-5815-4303-8732-4e11d6faca08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 243
        },
        {
            "id": "b800c04f-d8de-4f03-bc88-b4c4a6b38950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 244
        },
        {
            "id": "c18de5d2-cb1f-45bd-90ca-ea8926ed29ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 245
        },
        {
            "id": "2025e0a6-3764-4a86-abcc-b89f87838ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 246
        },
        {
            "id": "f5f7c21f-567b-4781-a756-2e02223d6147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 248
        },
        {
            "id": "bc776ca3-745b-4216-a949-47907d6a1762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 339
        },
        {
            "id": "5807a240-7c2b-4a45-90a3-7c8ea4a40b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8211
        },
        {
            "id": "1384668a-da02-49bb-b5f9-05965e5b8206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8212
        },
        {
            "id": "8c6da744-4022-4ef7-a294-e705c60e8076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "f033a3da-0c18-4541-9902-d22ca3016212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "43c64fe7-912b-4f00-ba3e-b2c277566ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "bd3933bb-d2b6-4713-865f-421471029dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "eb8964e2-9568-429f-afaf-d03ee48b7351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 55
        },
        {
            "id": "d71a2b7b-94a1-4906-ab8e-0b6c892c79c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 8216
        },
        {
            "id": "aa7deda3-e972-4c66-9bd6-f165a09777fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 8217
        },
        {
            "id": "66dfba43-dc89-445f-8350-5e2db0dd13b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 8220
        },
        {
            "id": "ef1d185b-bfe5-4b32-9d20-c4d8c484aca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 8221
        },
        {
            "id": "61d48e34-c341-4115-8fea-258f86daa02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 34
        },
        {
            "id": "7076bb71-c2cd-429b-90ac-28ef7f0d803c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 39
        },
        {
            "id": "8d72e98d-f912-48e0-aa27-b95c5f40ef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 55
        },
        {
            "id": "dc4e1219-79a1-4dec-a0ca-00d1a753ad06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 8216
        },
        {
            "id": "d3adf0d4-9308-4e74-b2ed-7b4fa965b981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 8217
        },
        {
            "id": "24addfec-864d-411e-b1c2-003d58ec7848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 8220
        },
        {
            "id": "0339d9fa-e1bb-4a11-a57a-7c7ee6d8fd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 8221
        },
        {
            "id": "3214ae0a-ad82-4353-bbea-4d5b444291bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 44
        },
        {
            "id": "1b5a8803-45d5-4d78-b0cf-2fc8e7004fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 46
        },
        {
            "id": "715cfcaf-d22c-4ad1-85a9-93e0ddf88be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 50
        },
        {
            "id": "7b21196a-7a50-48f0-a497-477c4b6054fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 51
        },
        {
            "id": "94921af9-997a-4142-9dcf-aa496bd6dfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8218
        },
        {
            "id": "c2b98bbc-31c3-4881-8fa1-c4f133518c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8222
        },
        {
            "id": "2af16112-1186-4cdd-a7dd-b6bed906282a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8230
        },
        {
            "id": "db34df67-2c21-4191-bb43-55f3658602fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "198632b9-0a83-40ee-b0d0-098e9cab9f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "7a38f3e0-8485-43e5-9d5a-1e447471e0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 34
        },
        {
            "id": "69b2859c-e367-435b-9788-d03d41cff089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 39
        },
        {
            "id": "31d230b8-9240-408f-95f3-c5b2bb754c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 45
        },
        {
            "id": "9dd8e41c-669f-4d06-b793-b7dcdbf5ff36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "5d380d5a-7f9b-4108-95f9-b45f3c3b4a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "af7642a8-dc88-459e-ba89-b61262a823de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "108402dd-b787-4e71-8bd0-2a99a1c0087e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "d34cede8-0764-442d-838b-ed98a576bcac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "804f0fa3-4ac9-4c22-88f7-25ebd5042625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "df08071b-b7f3-4adc-8a3b-0a7b48dfae80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "8fc1e1f1-dd70-43d6-a23e-4af835a75181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "7a20acad-d933-4e9c-af2a-461a6b2a9656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 960
        },
        {
            "id": "dc5bce9d-9236-4c5c-841c-7901ada3326c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8211
        },
        {
            "id": "25a429fb-0af0-4f26-a439-771f48f0f40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8212
        },
        {
            "id": "3379a1fd-4330-4402-86a0-068afb11cd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "1603e08c-3f03-4582-93e8-79e8dbbb9c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "d27a7b9e-dba8-400d-8a86-5a4027c78605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "61a583ee-2185-4cef-b567-0ad17865d132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8221
        },
        {
            "id": "b078e7ee-e91d-4781-814a-64659d9501af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "0043be28-8dfa-47ad-be1e-ec60117c4d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "cc92427d-0ef3-46f3-8e46-5a00ae9ab024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8211
        },
        {
            "id": "5eb56b44-79d3-4476-b3ec-b3bc6e6b649e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8212
        },
        {
            "id": "e2604c5b-8d3d-47d5-b2c0-f3cd80abe18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "b19adfa6-24ba-427b-8e97-47ded9abdcd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "ea8f077b-ddaa-41ad-be17-f247d3e32203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "b05ea728-1c2b-4dff-be87-e0cc4d4489de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "d131c82e-b347-4d83-92ad-3d1d35064e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "914fe9ed-8562-4328-9076-1af168646690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 74
        },
        {
            "id": "3b0aeb31-636d-40b0-af69-8af027c5fbae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 84
        },
        {
            "id": "60951251-5e9b-4ac9-8552-542409f746e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 86
        },
        {
            "id": "a90f10bb-5742-479e-9650-fa433ae9356c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "4ea8d68b-fa0b-4562-bf23-5b14fd0e065a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "a949b563-0f5f-448b-8176-586611f2aa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 89
        },
        {
            "id": "883e022f-3a16-47cf-b5b0-7dbdb1c80b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 90
        },
        {
            "id": "6562780f-95f0-4910-9ea8-fb4ceb0612f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "1f303203-af1c-4f51-a826-a2bb0f4a2151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "80854c24-c1b9-460f-9b3e-6698add0c73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "7483de1d-9361-4716-9ae3-b002a80be3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "a7422e6a-62db-48e0-b80b-dc4405d27711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "c4849544-e174-41dd-a831-391e2f667b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "0da9d261-e90c-444e-93ed-4e360291b208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "0e8e1ddf-c707-4887-bc12-cd15603df7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 376
        },
        {
            "id": "56321d69-8bd5-4d2a-95ee-a877bd77f9be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "341676ea-d9a2-44bf-b111-e3959ddce667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "61c41a90-9a45-45a1-a3c7-5fbde2eccc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "98af495c-6ecb-4dea-ad01-30f6a44a1878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "eb56f37f-5f5d-4ebc-889c-f38c6c9c15ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 86
        },
        {
            "id": "971ed540-e5d4-42ad-9db7-14d4029a539d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "3704f6ef-f956-4592-9ced-7972e1bc6604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "91b47ee6-c5b2-45d0-9ccf-34c56ba93b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 89
        },
        {
            "id": "d13ae534-acaf-4996-b47d-5bb994a07312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "d27796ee-83ee-49ad-b3cc-c01aa0910e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "eff9010c-34bc-4771-8b93-3998a817fa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "92e40b5d-f7ba-434d-9ec8-c2e22851bf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "fe1e6f06-dce3-4294-a78f-c13eeba05c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 376
        },
        {
            "id": "00f2e954-e507-4af2-ba71-763b7130b000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 960
        },
        {
            "id": "f24794ec-d3a0-4b73-8809-e5d93c1263d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "46f4beb9-5308-407b-9eab-006da1d3788f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 34
        },
        {
            "id": "c050cb79-fd51-4d67-810c-91f5cc55593b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "573d85c5-b05d-43b7-b940-96c74b61d9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 45
        },
        {
            "id": "500dd6d6-97bb-4405-847e-689336a5bc35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "7c450422-7667-4f69-8452-a0deaf4e12c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "c79f3ebf-b31c-48ff-ac26-99dfd4239539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "b87459ff-06d1-4ed1-a8f1-317f937e42c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 960
        },
        {
            "id": "b601d0d2-1c9a-4f3a-871c-59257814e1c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8211
        },
        {
            "id": "aa7c8cfb-a6d5-4737-a3b6-40c54ce6f4c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8212
        },
        {
            "id": "17ca8c67-24e6-4973-ade0-52e9cc720ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8216
        },
        {
            "id": "3372d2b1-a42d-4668-8df4-2681e442e88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8217
        },
        {
            "id": "352ab93e-52bb-4676-8fbe-89e58d2afe7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8220
        },
        {
            "id": "4648b539-f337-4357-ab59-a6abd1c8b2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8221
        },
        {
            "id": "e8776edd-1d1c-4740-9013-f51569646861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "1a8540bc-96cc-49e5-9707-1482aff585c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "bbb85c1a-2435-4c64-89d4-c8a451328a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "39369237-710f-4f16-b510-d434b44a207c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "669e03a5-391c-446a-bccf-e6ce6692f421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "e18cfed9-5e5c-4549-a6fd-4da2c63ad2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "f88c8e59-097e-4d1d-bca9-385a48d9c1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "4d8a61c2-4096-43f3-927a-af9fa4824222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "4fff0156-92b0-4858-9341-ea3d69e829af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 118
        },
        {
            "id": "7c2ee6cf-57e0-4794-ba8f-bf486cadc6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "9fae8d2a-bd5f-4250-9ab0-b12f6927b7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 960
        },
        {
            "id": "e3da0a48-c631-49f4-ba14-d85733b42ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8211
        },
        {
            "id": "f8d09bde-c2ed-4f90-bf41-f7cd717b08ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8212
        },
        {
            "id": "9b0906e6-3e79-4567-99fa-06c6c0a17566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "a03f5554-428f-4ed4-95fe-e988e374c749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "2bb96ddb-7ef9-4e60-9075-d302fede54cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "bd676955-bba7-4a88-9cc5-776692269e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "18109f79-b7aa-4533-9d4c-81a15df29784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "4958121d-aa34-470f-aa28-e9702712ddea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "4fe5f673-d0c7-4ca4-897b-320a3169d82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "8e32006e-2cc3-4c3c-93b9-7dee422108f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "d7993378-4726-4dfe-8264-836aa0b3595d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 63
        },
        {
            "id": "fff088cf-123d-4e10-a3bd-075418eecbd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "8ae731bd-c680-46cb-b2a3-db20df91e933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "d04674d0-b337-455c-948a-c7a6d2f1e2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "38a0ff93-b4a3-4364-9be5-56e8f28b59c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "912881c5-a49d-48fb-ae77-cde4399afdd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "6bfc4e2e-ca2e-450a-b426-06c6e178e495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "840e319a-9f11-47f8-897a-c1a114ce4d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "0566084c-4600-4631-ac7b-316c44d72f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "cbe670b6-9ad6-4f22-a372-c445593afcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "9dc7b765-1321-44e4-9a5e-47187280e73c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "ab2bda98-5bca-4aa6-b5c5-a9afb40deded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "a48363fe-61b7-44a5-b60a-5ca3d3f3455f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "bae2cbc2-711d-46a6-9b06-60a9e2591e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 226
        },
        {
            "id": "4695e64b-a6be-483d-ac9f-bd22224c3577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 227
        },
        {
            "id": "c4f3b56a-3703-4bc0-8241-a389868765ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 228
        },
        {
            "id": "980a5266-0820-415c-8cf4-c62fbc501123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "9e15e55c-8446-4cc8-ab62-4e7b371cac7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "134ba4ce-767e-4a24-8347-4c52f8ea0e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8218
        },
        {
            "id": "648929e5-1cfd-4428-9d33-9bc2b4be5391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8222
        },
        {
            "id": "eeeae8e4-d3f0-4ff3-ad7e-09dfae6395e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "37ed4c4e-ac01-437c-b7a9-ef41c049c9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "2ee26330-226b-46d1-8a91-e034819aa916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 44
        },
        {
            "id": "41d95378-757a-4cbe-bac3-ff7520da6c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 46
        },
        {
            "id": "4d25cef5-acdb-4a6a-80c9-fb40bee54824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "f046b452-a336-4444-9e33-66d14ea1b086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "11949d94-06a0-4c71-b37d-3491c0700362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "8bea961d-f576-41c5-8837-4b46f0c99022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 97
        },
        {
            "id": "f3f1e6e2-e3f6-4afe-9a99-7a869173220a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "c4c575a5-f1a7-40a7-ab1c-2d42cd19a55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 224
        },
        {
            "id": "62043f20-5211-4ec7-ab14-487b5b3c3c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 225
        },
        {
            "id": "9d5da0a3-9e8e-4878-8892-dc06b39c67fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 226
        },
        {
            "id": "e02c40d0-8851-455e-a251-ab93f2677588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 227
        },
        {
            "id": "5774d697-7ab0-41ac-975d-5d881945726c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 228
        },
        {
            "id": "f7de2300-6ca3-4d3e-a994-e634a6813023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 229
        },
        {
            "id": "2d7f47ee-e223-4e52-bd5d-8b3e717f1f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 230
        },
        {
            "id": "d1aff497-7b51-465f-b47f-032d46f91e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8218
        },
        {
            "id": "d921c476-6ee0-4b7c-94a1-f034659a587a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8222
        },
        {
            "id": "78cf9fa8-cd9b-48a2-9895-73c144ffc355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8230
        },
        {
            "id": "6733cfad-e33d-428b-bbaa-98fcfdaa7944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 34
        },
        {
            "id": "2f0dead1-9371-4295-8ccc-6346686c3818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 39
        },
        {
            "id": "b21aee73-fb99-4755-83f7-5b383ea029c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "30b48a30-bccb-4c3e-a460-db9f44862347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 86
        },
        {
            "id": "8de1d7d1-e789-4773-ae77-42f5de8e4e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "d5fadc26-05e8-4707-a93f-f97ac7fcb97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 89
        },
        {
            "id": "81fbb869-9159-4a8c-986d-1d9cfdc7085c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "419bb062-737f-4e45-9916-4c350621001d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "6a7f1a51-ed23-49ec-85f9-06cf27a9d021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "23aa7ccf-4f4d-4dbe-adb8-22d40d92f9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 376
        },
        {
            "id": "26ad0b9b-7321-47ff-81a7-0b144d1f2064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 960
        },
        {
            "id": "1d1c1b95-dc93-4850-a13a-39d2dcbfb791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8216
        },
        {
            "id": "dcb7a2e7-81ef-4fdd-a3df-cb6be247ff0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8217
        },
        {
            "id": "d98b4f7c-b24c-455a-9a9d-de6d0cbf4e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8220
        },
        {
            "id": "0f4ce73f-2989-425d-92bc-9165f2ddb20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8221
        },
        {
            "id": "925a3da6-f860-4044-a8e8-4177a65e7878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "e7029a3f-55a8-47b6-ba0b-f0448148b2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "9f419b32-12f2-4828-a0a8-30397f03897e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "ba18402c-5c60-4329-bb65-5aab6fff2ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 47
        },
        {
            "id": "68dc8042-9f0b-4b13-97aa-49cae3caf5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 63
        },
        {
            "id": "bd4a9ecd-9d94-4a00-b410-d1db5ae8b49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "935f8fa1-7c4d-49d5-91cc-74625e8a59a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "9d15f861-a2ab-46fc-a126-0758925bf1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "dd40ca23-b2bd-4308-8415-9f3dc6d690d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "fe6423a9-c255-4f41-9049-193b58cde642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "d2e25c99-38c2-4fdd-99fe-9614a556a538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "826ad53a-4cdc-4325-85ce-6bd21864d618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "017cfb5c-bd76-4e90-8e7b-46435b012d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "287e3849-77d7-4413-84ee-0cfbeddf35df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "ca3ab599-d08d-42a9-8208-e4db973736d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "ccb09eb5-7032-41d6-a18d-3cd5459949ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "ef059e88-50a4-43f1-a752-408ac98029f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "c779a9a2-01fd-46ec-b1e5-15d4644d985f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "cdcb6ad0-86e4-418e-b595-a4411da858de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "5ebfbe0e-ad20-4a92-918b-cf6ca657b460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "7707765e-f0ee-4f0e-a33b-8adb4ee57375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "aeffb5b0-b1c6-453f-b2c9-119f2ad3afbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "31ade4a6-5a73-485a-8e2b-c7b78c2dc7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "8dd83291-ad3e-4683-9c0b-a6280d630897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "1678a45d-4199-482a-8f46-7f9f6e78ac4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "6605cc3f-2b8d-4d79-93af-8b08b43ff5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 198
        },
        {
            "id": "f30257ba-6624-434e-b835-01c7ec57675f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "eb5f6c16-b157-46b8-802b-2deb52dfe266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "a236484c-9418-4388-9d02-490362384147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "4d0f3f93-f888-480a-ad9d-4632a637a3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "eda5b7dd-aa11-460c-84d5-6a8046bfae27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "9cb601ac-03a3-4185-aa98-e08910f95079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "a665dca0-b3e6-4734-9e23-840f2c79d14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "28935a69-d7f7-4953-9155-99fc17b74cbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 231
        },
        {
            "id": "ff0b128d-cf1e-48c8-aeeb-ad243b126c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 232
        },
        {
            "id": "e7637b2a-19af-47fc-ad9a-db38799be8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 233
        },
        {
            "id": "965383fa-3479-4bf6-95b5-fa617ea01121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 234
        },
        {
            "id": "73771918-0c69-43c8-acda-01bbaa2990b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 235
        },
        {
            "id": "59278f83-f086-4bae-9c41-766f074ce140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "6202ab8a-5285-4ef6-aebe-4aff6cb45263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 242
        },
        {
            "id": "c39c3fd6-60da-433e-80b6-7aeffec0c2a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 243
        },
        {
            "id": "8a3a0b0e-e92e-46fe-a1f5-b1e7d50c89f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 244
        },
        {
            "id": "96a286a8-a5c2-456d-967c-3821de46fbb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 245
        },
        {
            "id": "46485bc8-e5fc-4692-b792-f0c3a8df1bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 246
        },
        {
            "id": "61fcb226-7c18-4d68-95db-f889c7e31502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 248
        },
        {
            "id": "1fcd0280-c9c8-41c6-b990-eec22d47bb8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 339
        },
        {
            "id": "3b87d849-ed68-447c-b1c8-5312497c6825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "6603548c-fc4c-4d82-8d5b-31396d90d8e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "b8fca7b5-873a-4719-ab89-0428394c2c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8218
        },
        {
            "id": "6a502e85-cd1f-47de-a19a-2e8be5aa42f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8222
        },
        {
            "id": "ba855963-9fa2-471a-9f37-1544712899b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "a83d9568-56cc-4ab9-8013-108e6369fc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "06d9e795-fffa-4fc3-93bf-42a84f2624f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "a1f0c5da-9aca-4645-af00-58f8d110efe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "e5ad40be-6812-4f17-90c5-050c4eb77fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "bff1eff5-86a9-4c3a-b0f4-cae7202feb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 47
        },
        {
            "id": "63cd69d8-0fbd-48fe-8f20-bab71a5155e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 63
        },
        {
            "id": "ff37381e-53a5-436d-a9d9-4aa7876cc9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "0b064ae1-dfe6-4da6-b697-6af95c911114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "dea416b5-b4fe-43fc-a297-d4a955f120d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "4ab2702e-82b4-4d9d-b708-aaeb4c9fe17e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "bc3ee489-dd81-42a9-b46a-56f391d3bb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "7a0f37be-e9f0-4af7-8580-c7cae41e4f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "659534dc-c905-4961-8ab1-d05b37f72702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "f556f005-3116-41a4-b608-6427f383fd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "b5b6be66-3f50-4b3c-93d8-69bcb223b791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "efb82c9c-e2e9-405b-9b5c-6246b98f68b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "7c31e54c-26c8-4c16-a058-aedc4d47ef42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "4cb532aa-fc63-4554-9339-5f6a16c96c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "8bbc9b6b-7c73-4281-8310-834264fd6896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "8f419aed-9de9-4025-822a-bb826dd115c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "75dd4217-5f74-441b-af0c-90b2fc2ef6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "0f035431-f270-4dcd-9ca3-45663bcbfb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "1aa0c227-0495-4871-81ce-86c3da2ea59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "de2f01f0-73d0-4954-a345-bc14a4267786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 193
        },
        {
            "id": "727781d9-0d12-4caa-a3d0-ffb7345ac09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 194
        },
        {
            "id": "7b27a37f-aeb9-49a4-9e07-12322b03e281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 195
        },
        {
            "id": "13fe2d3b-a9d5-4a82-a10f-cf8e6af56188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "e1f6c56f-7d89-439c-b008-daa81dd98fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "de696420-ba1e-4e4e-aac2-49bc661f4ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 198
        },
        {
            "id": "c904fd69-0129-4bb6-bf6f-584969353d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "1dee548c-83b1-4c33-b360-7726a7bff4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "1e04aeb8-2847-4a39-917f-0f7942a56b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "4c9faf48-192c-4359-ba22-14e8fa6c3fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "34b79576-3f2f-4792-8246-3c48be78d602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "c5e03f74-8e6e-45b3-a38a-7f76794c8a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "edff28bf-1dc4-47dd-a49a-de15204dbd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "3ec8ad2b-1070-4655-8eff-aed51a34dfe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "cedc3d59-5107-4835-8b65-f262f3a60e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "3cebee35-526a-41c1-8ae3-fc21fbdaa639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "79acd996-1d6f-4585-b30c-3d5bf8fcfa05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "d6f77367-b95e-4694-8842-7feca87fe965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "54137bdc-4402-40df-a654-dc0e6ddb7a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "c1d3c202-14a1-4f70-96b1-cb152940d9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "def1dcce-ce65-44bb-9ca9-afdf295def80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "edf7d2e3-97a1-4e48-859f-9d59d2776cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "78ee370f-b81e-4557-9e3c-2afc528d1b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "5979bd39-68d0-48e3-a35f-39198c865c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "563179f2-d615-41f7-8e7b-9fa5b439e06e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "da88182a-b34b-4b7f-bb28-90b9bee8b9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "c4aaa1c5-dad7-41c7-883e-6c013b7ad590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "cfe90161-e2df-4c91-a977-6a473f48ef31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "5928357a-ed88-43f7-a2fc-4186f06ea7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "7062d0aa-c67a-45f2-a3e6-eb1a19d7d903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "0cc9b26e-b07b-4fe4-989b-79c4a639f72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "792ff239-fcac-4243-9890-5cac1dd8d57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "9d4f96f0-14fc-42a2-8843-f854c69ca563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "5261dfa6-90e2-4c1b-b776-fd30771ed612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "6103082c-4469-40b6-a55b-7c39750ef933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 47
        },
        {
            "id": "f0cd3679-da2f-43d3-a1ee-b9da4c6f26e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "633bc916-df06-43e4-93c1-bdbe5a426872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "7547bfb4-5cc3-4517-901c-43416c37a892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 70
        },
        {
            "id": "4ff2926f-89d2-4b00-95bd-6cf6e1cc2abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 74
        },
        {
            "id": "e7991f6b-4cfc-446c-8d64-c5b5ce61a269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "7ca36905-a2d5-4a7f-b8db-7b8af00470f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "4833910b-48c4-4cf9-8292-c316fc85305a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "73eaf37d-41a8-472c-be95-86e693060b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "aa75a5f3-0490-41a5-a6bd-b38789240c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "c00e97af-40cb-4524-b4e4-478e81a2aae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "81fedd5b-4790-4ba9-be59-22abfdfd3ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 109
        },
        {
            "id": "6df3b010-13b5-4c2a-beb0-8b0f0a9927b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 110
        },
        {
            "id": "faca69c3-6fb2-4e7a-ba88-b773c3e1745b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "ba82573d-2f2f-4943-91c6-dc766e88200b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 112
        },
        {
            "id": "25b3258c-51de-4c17-809b-a2301d5b43c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "3ec61539-a5d3-484b-afac-a08b81813b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "fad2e91d-0dcc-4d12-a8e6-af12920d52ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "0dd83639-ce2e-4e1d-892f-b41629f2ef57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 122
        },
        {
            "id": "2d429552-60a4-4680-b07e-1ec646fcc467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 193
        },
        {
            "id": "297ff39d-7cdd-4451-b6f5-24fbd3dc58c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 194
        },
        {
            "id": "5567f14f-da01-4332-8a08-46e31ca83ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 195
        },
        {
            "id": "547a14f9-1f3f-4b7c-96de-44c829ff440d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 196
        },
        {
            "id": "40a43f0c-1ac6-4d54-b896-8fbc1941c526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 197
        },
        {
            "id": "b59d17ef-673e-4fdd-a9fb-6baba00e3628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 198
        },
        {
            "id": "3046a074-f720-4502-81ec-b26d208d144f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "b4d00f3a-a4d2-49a8-8c65-29f2422ee065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "0bde1d3f-c4b4-48eb-b502-283d8812dd24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "28055a68-6338-4ec5-93a5-2ca66037b018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "1312fa67-4f12-4195-934c-ada46e631c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "063cd554-c9c9-4df7-bfc0-8b7d0ef879e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "c51156c1-7326-426f-be23-face70e7c6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "c40e7cf3-7ef4-4108-aa6b-733c24150ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "73b62c38-3e03-4c02-8408-1cd82f1a4a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "5c1d5b7c-9499-47a2-8c62-9a447093b046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "b607c362-fc00-4974-82f9-fff9b69d9dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "b79ff573-7220-44f5-960e-2b191e645891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "35e527a4-ccb2-4f93-a29c-52a375186b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 241
        },
        {
            "id": "5c31892f-6875-4d30-a2cb-1509d62ab896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "86a31b11-eab2-4d3d-8556-86e09044b41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "7e3b19e6-b0d3-4bd0-9870-11703489be34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "2680d331-4b66-449c-9a05-b6f335aa9b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "a3bceeb8-884b-414c-b3d1-6ccd7721565a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "c50d5162-5d24-49fa-83b9-0c0777cfe0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "acb31ea3-dca3-4181-a1bb-fadde5776a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "27567d8c-e889-49e4-ab9e-843e5c07dd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8211
        },
        {
            "id": "5fcefa47-1f85-4a6d-bb40-6e61f59affe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8212
        },
        {
            "id": "497f806d-0a83-42ce-ac7d-f59205cb695a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "1724fe5b-bdb2-4999-a380-539245dc1482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "c3e1df73-2100-4a96-a768-588a058295d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "a46bbed2-050e-4580-ab69-69461cb77779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "060d45a3-eea4-42ca-bdd2-cc2a89edba7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "f6f76192-37fe-4703-a90b-1113746eb2b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "8b2cf439-05a2-465e-a4ad-cbfe50dd2986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "e66b6e4a-b984-4d8c-a212-906cd8e66e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "af2d74c0-a946-4c06-8229-da1a30692736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "4b75ed2d-1bd0-4d7e-a6d8-6bb87c2802c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "529ec47c-2c24-4861-8e8f-e2bd00837e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 63
        },
        {
            "id": "b190f137-72d5-407b-ba06-bb6f0cf7cb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "495f85c2-df52-46d5-90c3-e284101051be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "05f18d94-3945-4d80-9873-adc82a667d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "802ee476-6c0a-45da-a4bf-2752db4e2488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "bb6ad544-3f3f-4a28-8c34-a79ac095eb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "83cf3bc2-7e12-4454-a141-678ad40d7440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "fc256f71-142b-409c-bd71-65be24f668b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "b868d184-022f-4b08-bb22-81863fd690fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "02a7f49f-8594-4d75-ad67-3c1e7c6009fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 109
        },
        {
            "id": "9cf25de4-04e0-4708-97b0-dc3e25ebd615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 110
        },
        {
            "id": "e8061e50-4e5c-48aa-856f-3033b4ac8fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "7a13a82d-1f62-4f8d-a187-34d70c743830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "deecaeb2-05c8-4ae7-aef5-53c9bbd63438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "dea656d8-2194-4975-af71-2c7b87a95123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 114
        },
        {
            "id": "f52d0c28-d570-46ec-9075-ea27f9c4ff4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "158aa4b2-ea0a-43a3-a025-361eaefa977a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "98bc6233-37a3-4669-831d-31aafdc73674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "4655a5b9-4c61-441c-b8ab-c9a9eac653c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "5c5e6480-089e-45df-806a-c09c86eb2477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "e4a3f82e-45be-4819-8869-336c881f2e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "7819cb04-5887-4360-9925-3767515e1833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "f2ef2b41-fb0d-4f6a-a8de-ce2e7708bf2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "f1371881-dea2-4aa3-8118-eabf3df248b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 198
        },
        {
            "id": "4d90104b-acf4-418a-b631-0b588e91d12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 224
        },
        {
            "id": "f7b65e48-a614-4420-a0ed-b12477e5d8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 225
        },
        {
            "id": "fa91e5d9-ed4f-4c94-ab29-3fc82927455a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 226
        },
        {
            "id": "dd4a200b-86e2-4a1d-a82b-129e42706eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "9a8bcaa8-f8df-43f8-8182-f4cc49622dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "bcb32f00-d791-4508-a7f8-a1bf98010685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 229
        },
        {
            "id": "afa646b0-961a-45a5-b4f8-b3c17952f4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "f5bc390f-4d17-4d0c-9ffa-2a20b1d2ca17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "8de0c1b1-8249-45c8-941d-78a8116c8024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "95e36993-154b-48dd-858e-253b9539c7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "549de53d-38d1-46ea-b7a3-efd3fad6972d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "5884342b-86b7-4e8d-aae7-929b2be39c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "3a63ce41-a05d-41e6-9bb5-c1f440f0406b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 241
        },
        {
            "id": "4109d762-d24e-45a0-b53a-53e319623ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "d3fd6de8-2256-4bdb-9972-bdf3347bb13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "47217c34-a093-4bbc-a5c0-f25f487166c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "e0d1458a-ba58-4145-a060-0346baaf0454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "f07ecdb1-040b-4e6a-9a0f-022a62e1aab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "2a8a35e7-cd3f-41d1-b04b-d8b1c591b0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "b9d72d20-a2ff-44e8-b6ad-dd1dff7faf46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "63fa44b5-6ea3-4e48-8407-46038b2312b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8211
        },
        {
            "id": "84c79760-c982-4a9f-bd38-cd1f6a7d844f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8212
        },
        {
            "id": "1eb65e63-12f0-45c7-b10e-655497832f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8218
        },
        {
            "id": "47170b1f-8d6d-4009-a485-c7db52a09ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8222
        },
        {
            "id": "a3b0de91-f494-4efe-a7a4-f213e97f6dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "5447c286-b35d-4e1a-84d9-93129028f646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "43b84af6-8f15-4bd2-ac20-55e42a6071bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "b2eca143-e544-46ad-96f8-0235e3734c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "341ee7cf-078a-402f-b086-3046801d4aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8211
        },
        {
            "id": "d7969763-31c4-406b-897b-2548a5d88308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8212
        },
        {
            "id": "e94c9089-b01d-4c18-a4f5-b83f08824121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 91,
            "second": 106
        },
        {
            "id": "22621928-425a-4320-a3e0-29bc8ba3a105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "9d3440e6-35f3-4046-b1d9-3857189cc182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "57210d1a-b9bb-400d-91aa-b9d7a3b314be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 44
        },
        {
            "id": "c72f2be3-e8c8-43a1-8419-f4b93302aa41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 46
        },
        {
            "id": "b217ea72-fa6e-419c-ab88-775a2b4dfa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "96a117dd-98d8-4b6a-907f-1e78b00c9595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8218
        },
        {
            "id": "2bb772e2-a879-472c-81a1-4f13496fbb45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8222
        },
        {
            "id": "7b1c9ef1-34ed-40f9-bc2e-d8e4919c5d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8230
        },
        {
            "id": "5508f953-8ef9-4f5b-b5e8-396c0c184292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "6522f589-b093-4c94-b59b-4ecf194c8620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "44ac694f-cb8b-4c42-ac4f-eb53437d5a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "ddd6dd31-95c5-4494-bdc0-580f313563d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 63
        },
        {
            "id": "8c933b05-e19c-41eb-bd34-ae81f2bc4372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 97
        },
        {
            "id": "a84874f9-93a1-42de-be72-aa928bda639b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 106
        },
        {
            "id": "2e66fcf4-366b-4274-adea-bba5df6fa4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 224
        },
        {
            "id": "d831c37a-1533-4bd3-95f0-ca36055c369d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 225
        },
        {
            "id": "230b26ba-00c2-4ce3-9c07-08b2d722b76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 226
        },
        {
            "id": "85000f64-a1db-4029-8084-a1ce7fd2a3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 227
        },
        {
            "id": "fbf398ba-4162-4404-b029-79d8f11755fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 228
        },
        {
            "id": "c058e1a5-a769-4316-bca2-178ed03da25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 229
        },
        {
            "id": "12a4b294-c056-4ae2-8548-7e7ab034679d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 230
        },
        {
            "id": "54d25bc9-fea2-41b0-8682-921a22231f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8218
        },
        {
            "id": "882d2450-090c-4ad2-8df6-5915d58695db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8222
        },
        {
            "id": "9e601876-37b8-40bb-ab14-728979c513fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "5b204112-0960-4782-add7-7747fc60d481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "951271b7-c0c2-4ba2-8c6b-a55d967dd7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "4244d358-58cc-4631-82e6-f6d6bcb36e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 38
        },
        {
            "id": "149b34a6-1d40-4573-afae-b594c50a5566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "89dd2fe4-d505-42df-8f73-f6c1a0949534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "e05edaa6-e88b-46f0-92e7-485ac4fa8088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "5d90916b-6759-4d8b-b480-08101f894123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "54110c56-e76a-4d27-b8a6-4d369dd4c1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "abb073f6-0b08-40bc-8084-41a3422e4cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 106
        },
        {
            "id": "f20ce296-fa7a-49f3-9bcf-4305dd4a0130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 224
        },
        {
            "id": "fa43ca4d-bff5-42ee-a398-0f265927d1ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 225
        },
        {
            "id": "61d757ac-6ec0-4ed6-a33e-20f33ce30120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 226
        },
        {
            "id": "0fe7defb-f256-4e4f-9cbb-bcd76d0598b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 227
        },
        {
            "id": "f13ea637-642f-4440-84f8-3e071a9a7cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 228
        },
        {
            "id": "248132a6-f053-493d-afcc-3e04261ca89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 229
        },
        {
            "id": "9094c7a8-b68f-45a6-a29e-effdad9c59f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 230
        },
        {
            "id": "4ee42720-f33a-48b8-8bd5-3eb8efb3ac70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8218
        },
        {
            "id": "16deee39-8fba-41dc-8392-a21adb04f436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8222
        },
        {
            "id": "b881510c-ab54-4d57-8c3e-c4c5d6a301aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "65dc9f89-da7f-4e2c-a156-d7ae65b01905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 34
        },
        {
            "id": "b538e1af-9be9-42b4-88a3-68de2fac7b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 39
        },
        {
            "id": "2f1ae0f1-535a-495d-899e-b1b11bf054e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 116
        },
        {
            "id": "ba531796-b256-4ab6-8c23-d5dd7f1d46ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "1597509a-3df1-4c1a-a612-97422091cd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "13e1659c-4818-4096-ab3f-ae45c4ea405f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 960
        },
        {
            "id": "df263040-8c8e-4aa6-bfdc-9c3f67eb2df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 8216
        },
        {
            "id": "c5c45c90-fe2a-4786-8d27-a647361dc199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 8217
        },
        {
            "id": "e8d8f583-b954-4dd7-a903-d76ffb7623a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 8220
        },
        {
            "id": "dbb5f0c5-3660-45ad-923f-3d1ddb1d7c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 8221
        },
        {
            "id": "c42066a6-2257-4135-b666-e761eecea1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 38
        },
        {
            "id": "e8fc9747-cdaf-4ce3-bb9d-6f8c160e3983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 44
        },
        {
            "id": "e3990808-c53c-4c85-a8cf-fbd732d3b686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 46
        },
        {
            "id": "cc467038-4925-4129-ad6f-19388d478a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "2a185574-14a2-4f1b-adf1-26d793f5e958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "7ee5c806-50c9-4b6c-90f2-0fe6325bc035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 97
        },
        {
            "id": "828ff96c-bd89-46d2-8946-030277f1d4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "53ed6d6d-4129-4752-a339-e12fdd1cd662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 224
        },
        {
            "id": "3e3ac9eb-edf8-4c43-8c85-3527c98ec7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 225
        },
        {
            "id": "4baee916-d16e-4995-8c6b-290bbb001c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 226
        },
        {
            "id": "206ce89b-9ba6-4ab0-8306-7aca65fbabee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 227
        },
        {
            "id": "6b2e9298-a4d3-4a3f-81c4-a1e9c894e3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 228
        },
        {
            "id": "0ef64119-9bf2-48c4-8061-b143d6b4bb06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 229
        },
        {
            "id": "d3c72fb8-16ef-48fe-847c-c9817a897a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 230
        },
        {
            "id": "3a015310-878c-4e95-afd0-5f631d1522fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8218
        },
        {
            "id": "f5cf3ea2-91fd-4a96-969c-8591e014355e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8222
        },
        {
            "id": "b286a043-e000-4ed3-ace3-019eebb44df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 8230
        },
        {
            "id": "2f906a88-375c-47b1-8f63-7c0719a914a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "4b862021-bb6a-4232-8d7e-878dadcfb5e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "006e350d-8367-41ab-bfd1-35eabc4c1a37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "b4ea209e-c0bb-4684-b8be-69705d00f228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "9b2ddd71-611f-4fab-80f5-976fb992b470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "1951bad4-2bb3-481e-916c-2e02d8183d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "240a9518-08fb-4dca-9e82-3e804a02a16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "387e9190-d633-49b2-88bf-28ab79d9050d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 224
        },
        {
            "id": "db4e1b8d-dc78-492c-9ea9-b736a41eb6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 225
        },
        {
            "id": "6b114f03-7064-492e-96d5-9694393cafba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 226
        },
        {
            "id": "9ba98561-ab4a-430e-9e43-400b8f5ac676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 227
        },
        {
            "id": "1bbbedc6-c261-4f21-91c6-4f6bf51f8438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 228
        },
        {
            "id": "e0b32a57-8e03-4794-9185-fac3f6406a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 229
        },
        {
            "id": "f7205519-4935-4f3d-ae01-7a8e2e846a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 230
        },
        {
            "id": "7523b3f0-d666-4917-9528-75b0ff57f19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "34a0588c-7e1b-4d02-9494-f58c733e3892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "29edfa95-c72d-4b86-a42a-ab4be142f504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "6d0c686a-bb23-40ae-9519-51c6e7d4c66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 47
        },
        {
            "id": "2ac1a68f-785d-4e11-b89f-502922b5a45d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "c2c774d3-a6af-4de1-aa67-b6f46de18b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "70496f23-8c23-4818-92c4-875103afb853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "587e0e54-ee4f-4439-8e7f-eca1431bd3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8211
        },
        {
            "id": "ad5a052e-0bcc-44f4-8cfe-ef5b8aa99e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8212
        },
        {
            "id": "09065f52-a214-407c-93cb-095292c20213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}